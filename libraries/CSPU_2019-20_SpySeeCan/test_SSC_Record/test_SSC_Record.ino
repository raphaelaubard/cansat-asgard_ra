/*
    test_SSC_Record.ino

    Test program for class SSC_Record.
*/
#include "SSC_Record.h"
#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"

#define TEST_HUMAN_READABLE_PRINT  // undefine to avoid testing non operational code. 

unsigned int numErrors = 0;

void requestVisualCheck() {
  char answer = ' ';
  Serial << ENDL;
  // remove any previous character in input queue
  while (Serial.available() > 0) {
    Serial.read();
  }

  while ((answer != 'y') && (answer != 'n')) {
    Serial << "Is this ok (y/n) ? ";
    Serial.flush();
    // remove any previous character in input queue
    while (Serial.available() == 0) {
      delay(300);
    }
    answer = Serial.read();
  }
  if (answer == 'n') {
    numErrors++;
  }
  Serial << ENDL;
}

class SSC_Record_Test {
  public:
    SSC_Record_Test(SSC_Record& theRecord) : record(theRecord) {}
    void testIndividualPrints() {
      Serial << "Printing a CSV Float with NO final separator" << ENDL;
      record.printCSV(Serial, (float) 12.3);
      requestVisualCheck();
      Serial << "Printing a CSV Float with a final separator" << ENDL;
      record.printCSV(Serial, (float) 12.3, true);
      requestVisualCheck();

      Serial << "Printing a CSV Float with NO final separator, with 3 trailing decimals" << ENDL;
      record.printCSV(Serial, (float) 12.3, false, 3);
      requestVisualCheck();
      
      Serial << "Printing a CSV Float with NO final separator, with 1 trailing decimals" << ENDL;
      record.printCSV(Serial, (float) 12.321, false, 1);
      requestVisualCheck();

      bool mybool = true;
      Serial << "Printing a CSV bool with NO final separator" << ENDL;
      record.printCSV(Serial, mybool, false);
      requestVisualCheck();
      Serial << "Printing a CSV bool with a final separator" << ENDL;
      record.printCSV(Serial, mybool, true);
      requestVisualCheck();
    }
    SSC_Record& record;
} ;

SSC_Record record;

void assingDiffrentRF_Values(SSC_Record& rec, int testCase /*1, 2, 3 or 4*/) {
  if (testCase == 1) {
    /*
      Power data:
      [0]  -10.0
      [1]  -10.1
      [2]  -10.2
      ...
      [9]  -10.9
      [10] -11.0
      [11] -11.1
      ...
      [44] -14.4
    */
    for (int i = 0; i < rec.RF_POWER_TABLE_SIZE; i++) {
      rec.setRF_Power(i, (-10 - ((float)(i)) / 10));
    }
  }
  else if (testCase == 2) {
    /*
      Power data:
      no data
    */
    for (int i = 0; i < rec.RF_POWER_TABLE_SIZE; i++) {
      rec.setRF_PowerToNoData(i);
    }
  }
  else if (testCase == 3) {
    /*
      Power data:
      [0]  -10.0
      [1]  -10.1
      [2]  -10.2
      ...
      [9]  -10.9
      [10] -11.0
      [11] -11.1
      ...
      [43] -14.3
      [44] no data
    */
    for (int i = 0; i < rec.RF_POWER_TABLE_SIZE - 1; i++) {
      rec.setRF_Power(i, (-10 - ((float)(i)) / 10));
    }
    rec.setRF_PowerToNoData(rec.RF_POWER_TABLE_SIZE - 1);
  }
  else if (testCase == 4) {
    /*
      Power data:
      [0]  -10.0
      [1]  -10.1
      [2]  -10.2
      ...
      [29]  -10.9
      [30] -11.0
      [31] no data
      ...
      [43] no data
      [44] no data
    */
    for (int i = 0; i < rec.RF_POWER_TABLE_SIZE - 14; i++) {
      rec.setRF_Power(i, (-10 - ((float)(i)) / 10));
    }
    for (int i = rec.RF_POWER_TABLE_SIZE - 14; i < rec.RF_POWER_TABLE_SIZE; i++) {
      rec.setRF_PowerToNoData(i);
    }
  }
  else {
    Serial << "testCase should be either 1, 2, 3 or 4" << ENDL;
  }
}

void assignValues (SSC_Record& rec) {
  rec.timestamp = 1;

  rec.newGPS_Measures = true;
  rec.GPS_LatitudeDegrees = 25.25;
  rec.GPS_LongitudeDegrees = 26.26;
  rec.GPS_Altitude = 27.27;
#ifdef INCLUDE_GPS_VELOCITY
  rec.GPS_VelocityKnots = 28.28;
  rec.GPS_VelocityAngleDegrees = 29.29;
#endif

  rec.temperatureBMP = 36.36;
  rec.pressure = 37.37;
  rec.altitude = 38.38;
  rec.temperatureThermistor1 = 39.39;
  rec.temperatureThermistor2 = 40.40;
  rec.temperatureThermistor3 = 41.41;


  rec.setStartFrequency(440.0);
  rec.setFrequencyStep(1.0);
  assingDiffrentRF_Values(rec, 4);

}




void performTest() {
  assignValues(record);
  SSC_Record_Test friendClass(record);
  friendClass.testIndividualPrints();

  Serial << "Testing assignement of different RF_Power data" << ENDL;
  assingDiffrentRF_Values(record, 1);
  record.printCSV(Serial);
  requestVisualCheck();
  assingDiffrentRF_Values(record, 2);
  record.printCSV(Serial);
  requestVisualCheck();
  assingDiffrentRF_Values(record, 3);
  record.printCSV(Serial);
  requestVisualCheck();
  assingDiffrentRF_Values(record, 4);
  record.printCSV(Serial);
  requestVisualCheck();
  Serial << ENDL;

  Serial << "Values Assigned (printing complete record in CSV format (with header):" << ENDL;
  record.printCSV(Serial, SSC_Record::DataSelector::All, SSC_Record::HeaderOrContent::Header);
  Serial << ENDL;
  record.printCSV(Serial);
  requestVisualCheck();

  Serial << ENDL << "Testing SSC_Record Clear(): " << ENDL;
  record.clear();
  record.printCSV(Serial);
  Serial << ENDL;
  DASSERT(record.newGPS_Measures == false); DASSERT(record.timestamp == 0);
  DASSERT(record.GPS_LatitudeDegrees == 0); DASSERT(record.GPS_LongitudeDegrees == 0); DASSERT(record.GPS_Altitude == 0);
  DASSERT(record.temperatureBMP == 0); DASSERT(record.pressure == 0); DASSERT(record.altitude == 0);
  DASSERT(record.temperatureThermistor1 == 0); DASSERT(record.temperatureThermistor2 == 0); DASSERT(record.temperatureThermistor3 == 0);
  for (int i = 0; i < record.RF_POWER_TABLE_SIZE; i++) {
    DASSERT(!record.hasRF_PowerData(i));
  }
  Serial << "Values checked. " << ENDL;

#ifdef TEST_HUMAN_READABLE_PRINT
  assignValues(record);
  Serial << "Printing complete record in Human Readable Format:" << ENDL;
  record.print(Serial);
  Serial << ENDL;
  requestVisualCheck();

  Serial << "Printing complete record except secondary mission in Human Readable Format:" << ENDL;
  record.print(Serial, SSC_Record::DataSelector::AllExceptSecondary);
  Serial << ENDL;
  requestVisualCheck();
#endif
  Serial << "End of job. Total number of errors:" << numErrors <<  ENDL;
  Serial << "Size of SSC_Record is " << sizeof(SSC_Record) << " bytes." ;
}

void setup() {
  Serial.begin(115200);
  while (!Serial);
  Serial << "Test of SSC_Record. Current size is " << sizeof(SSC_Record) << " bytes. " << ENDL;
  Serial << "(max size of XBEE frame payload is 92 bytes)." << ENDL;
  Serial << "(size of float = " << sizeof(float) << ")" << ENDL;
  Serial << "(size of uint8_t = " << sizeof(uint8_t) << ")" << ENDL;
  if (sizeof(SSC_Record) > 92) {
    Serial << "WARNING: max size EXCEEDED !! ***" << ENDL;
  }
  assignValues(record);
  performTest();
}

void loop() {}
