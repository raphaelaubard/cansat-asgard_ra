#include "servoLatch.h"
#include "CSPU_Debug.h"
#define DBG_LOCK
bool ServoLatch::begin( const uint8_t PWM_Pin) {
  thePWM_Pin = PWM_Pin;
  return true;
}



void ServoLatch::unlock() {
  myServo.attach(thePWM_Pin);
  myServo.write(UnLockPosition);
  myServo.detach();
}


void ServoLatch::lock() {
  DPRINTS(DBG_LOCK, "ServoLatch::lock, using pin#");
  myServo.attach(thePWM_Pin);
  myServo.write(LockPosition);
  myServo.detach();
}
