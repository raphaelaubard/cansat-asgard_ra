/*
 * XBeeClient_Utilities.cpp
 *
 * A couple of utility methods not required for operation. They are located in a separated
 * compilation unit to avoid unnecessary inclusion in the executable.
 *
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "XBeeClient.h"

#ifdef RF_ACTIVATE_API_MODE

#define DEBUG_CSPU
#include "DebugCSPU.h"

void XBeeClient::displayFrame(uint8_t* data, uint8_t dataSize) const
{
  Serial << "--- Frame content (size=" << dataSize << ") ---" << ENDL;
  for (int i = 0; i< dataSize ; i++)
  {
	  Serial.print(data[i], HEX);
	  Serial << " ";
	  if (((i+1)%10) == 0) Serial << ENDL;
	  else if (((i+1)%5) == 0) Serial << "  ";
  }
  if ((dataSize % 10) != 0) {
	  Serial << ENDL;
  }
  Serial << "--- End of frame content ---" << ENDL;
}

void XBeeClient::displayString(uint8_t* data, uint8_t dataSize) const
{
  uint8_t type = data[0];
  const char* s = (const char*) (data + 2);
  Serial << "String message: type=" << type << ", '" << s << "'" << ENDL;
  Serial << "length=" << strlen(s) << ", size=" << dataSize;
  if (strlen(s) + 3 != (size_t) (dataSize)) {
	Serial << ENDL << "*** Error: inconsistent size (" << strlen(s) + 3
		   << " vs " << dataSize  << ")" << ENDL;
	// byte 0 = type, 1=unused, last byte = \0.
  }
  if ((strlen(s) > 0) && (s[strlen(s) - 1] == '\n')) {
	Serial << "(last character is an ENDL)";
  }
  Serial << ENDL;
}
#endif


