// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "TorusSecondaryMissionController.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"

#include "TorusConfig.h"
#include "CansatConfig.h"
#include "TorusInterface.h"
#include "BMP_Client.h"

#ifndef INCLUDE_REFERENCE_ALTITUDE
#error "The symbol INCLUDE_REFERENCE_ALTITUDE should be defined for the use of the TorusSecondaryMissionController"
#endif

#ifndef INCLUDE_DESCENT_VELOCITY
#error "The symbol INCLUDE_DESCENT_VELOCITY should be defined for the use of the TorusSecondaryMissionController"
#endif

#define DBG_DIAGNOSTIC 1
#define DBG_FLIGHT_PHASE_EVALUATION 0
#define DBG_CTRL_INFO_EVALUATION 0
#define DBG_SERVO_POSITION_CTRL 0

TorusSecondaryMissionController::TorusSecondaryMissionController() {}

bool TorusSecondaryMissionController::begin(TorusServoWinch& theServo) {
	this->theServo = &theServo;
	return true;
}

void TorusSecondaryMissionController::run(TorusRecord& record) {
	// Check if begin was called
	if(theServo == NULL) {
		DPRINTSLN(DBG_DIAGNOSTIC, "Pointer to the servo is NULL: can't call TorusSecondaryMissionController::run(). (Possibly because TorusSecondaryMissionController::begin() hasn't been called yet)");
		return;
	}

#if 0 // Ref altitude now moved to BMP_Client
	// Reference altitude calculation
	updateReferenceAltitude(record);
#endif
	
	// Update controller info
	updateControllerInfo(record);
	
	// Update flight phase
	updateFlightPhase(record);
	
	// Update the servo position
	updateServoPosition(record);
	
	// Read servo data
	theServo->readData(record);
}

void TorusSecondaryMissionController::updateFlightPhase(TorusRecord& record) {
	// (No binary search implementation as there will never be more than 14 phases)
	
	// If flight phase shouldn't be updated, juste write previous value
	if(!shouldUpdateFlightPhase) {
		record.flightPhase = flightPhase;
		return;
	}
	
	// If not in nominal descent, flight phase is 15
	if(record.controllerInfo != TorusControlCode::NominalDescent) {
		record.flightPhase = 15;
	}
	
	else {
		// Evaluate phase
		float relativeAltitude = record.altitude - record.refAltitude;
		for(int i = 0; i < TorusNumPhases; i++) {
			if(relativeAltitude >= TorusPhaseAltitude[i]) {
				record.flightPhase = i;
				DPRINTS(DBG_FLIGHT_PHASE_EVALUATION, "Current evaluated flight phase: "); DPRINTLN(DBG_FLIGHT_PHASE_EVALUATION, record.flightPhase);
				break;
			}
		}
		
		// This check is included as a safety in case no phase was found previously: it should never actually come here because this check is already performed in the updateControllerInfo method.
		if(record.altitude - record.refAltitude < TorusPhaseAltitude[TorusNumPhases-1]) {
			record.controllerInfo = TorusControlCode::GroundConditions;
			controllerInfo = record.controllerInfo;
			record.flightPhase = 15;
			DPRINTSLN(DBG_DIAGNOSTIC, "WARNING: No flight phase could be found by TorusSecondaryMissionController::updateFlightPhase() although in nominal descent. Setting flight phase to 15 and controller info to GroundConditions.");
		}
	}
		
	// Update stored flight phase
	flightPhase = record.flightPhase;
	
	// Reset shouldUpdateFlightPhase property
	shouldUpdateFlightPhase = false;
}

#if 0 // Ref altitude now moved to BMP_Client
void TorusSecondaryMissionController::updateReferenceAltitude(TorusRecord& record) {
	// If invalid altitude, set previous value and return immediately.
	if(record.altitude == BMP_Client::InvalidAltitude) {
		record.refAltitude = refAltitude;
		return;
	}
	
	if(record.timestamp - lastCallToUpdateReferenceAltitude > 5*CansatAcquisitionPeriod
		|| maxAltitudeInLastCycle - minAltitudeInLastCycle > NoAltitudeChangeTolerance) {
		// Not been called for too long or variation too large, reset measurement cycle
		
		DPRINTSLN(DBG_REFERENCE_ALITITUDE_CALCULATION, "Reference altitude measurement cycle reset.");
		
		refAltitudeCycleBegin = record.timestamp;
		
		minAltitudeInLastCycle = record.altitude;
		maxAltitudeInLastCycle = record.altitude;
	}
	else {
		minAltitudeInLastCycle = min(minAltitudeInLastCycle, record.altitude);
		maxAltitudeInLastCycle = max(maxAltitudeInLastCycle, record.altitude);

		if(record.timestamp - refAltitudeCycleBegin >= NoAltitudeChangeDurationForReset) {
			// Cycle elapsed
			refAltitude = (minAltitudeInLastCycle + maxAltitudeInLastCycle)/2;
			DPRINTS(DBG_REFERENCE_ALITITUDE_CALCULATION, "Reference altitude set to "); DPRINTLN(DBG_REFERENCE_ALITITUDE_CALCULATION, refAltitude);
			
			refAltitudeCycleBegin = record.timestamp;
			
			minAltitudeInLastCycle = record.altitude;
			maxAltitudeInLastCycle = record.altitude;
		}
	}
	
	record.refAltitude = refAltitude;
	lastCallToUpdateReferenceAltitude = record.timestamp;
}
#endif

void TorusSecondaryMissionController::updateControllerInfo(TorusRecord& record) {
	// If invalid altitude, set previous value and return immediately.
	if(record.altitude == BMP_Client::InvalidAltitude) {
		record.controllerInfo = controllerInfo;
		return;
	}
	
	// Starting up
	if (record.timestamp < TorusMinDelayBeforeFlightControl) record.controllerInfo = TorusControlCode::StartingUp;
	
	// Startup done
	else {
		// Update controller info only if delay has passed
		if (record.timestamp - lastCtrlInfoUpdate >= TorusMinDelayBetweenPhaseChange) {
			lastCtrlInfoUpdate = record.timestamp;
			shouldUpdateFlightPhase = true;

			// ground conditions
			if(record.altitude - record.refAltitude < TorusMinAltitude) record.controllerInfo = TorusControlCode::GroundConditions;
			
			// not descending
			else if (record.descentVelocity < TorusVelocityDescentLimit) record.controllerInfo = TorusControlCode::NotDescending;
			
			// if below last phase altitude, we are in ground conditions
			else if (record.altitude - record.refAltitude < TorusPhaseAltitude[TorusNumPhases-1]) record.controllerInfo = TorusControlCode::GroundConditions;
			
			// too fast
			else if (record.descentVelocity > TorusVelocityUpperSafetyLimit) record.controllerInfo = TorusControlCode::HighDescentVelocityAlert;
			
			// too slow
			else if (record.descentVelocity < TorusVelocityLowerSafetyLimit) record.controllerInfo = TorusControlCode::LowDescentVelocityAlert;
			
			// nominal descent
			else record.controllerInfo = TorusControlCode::NominalDescent;
		}
		
		// Set previous controller info if delay hasn't passed
		else record.controllerInfo = controllerInfo;
	}
	
	// Update stored controller info
	controllerInfo = record.controllerInfo;
	
	DPRINTS(DBG_CTRL_INFO_EVALUATION, "Controller info set to "); DPRINTLN(DBG_CTRL_INFO_EVALUATION, (int)controllerInfo);
}

void TorusSecondaryMissionController::updateServoPosition(TorusRecord& record) {
	// If invalid altitude, don't do anything
	if(record.altitude == BMP_Client::InvalidAltitude) {
		return;
	}
	
	// Do not wait delay if reset should be performed
	if(record.controllerInfo != TorusControlCode::NominalDescent) {
		theServo->resetPosition();
		DPRINTSLN(DBG_SERVO_POSITION_CTRL, "Servo position reset.");
	}
	
	
	// Check if delay after target reached has passed
	if(theServo->isAtTarget() && theServo->elapsedSinceTargetReached() >= TorusMinDelayAfterTargetReached) {
		// Set desired position if in nominal descent
		if(record.controllerInfo == TorusControlCode::NominalDescent) {
			theServo->setTarget(ServoWinch::ValueType::ropeLength, TorusPhaseRopeLen[record.flightPhase]);
			DPRINTSLN(DBG_SERVO_POSITION_CTRL, "Servo position updated to desired value (currently in nominal descent)");
		}
	}
}
