/*
 * RT_CanCommander.cpp
 */
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "RT_CanCommander.h"
#include "CansatXBeeClient.h" // Even in transparent mode, for the macro's.

#define DBG_DIAGNOSTIC 1
#define DBG_READ_MSG 0
#define DBG_GET_PARAM 0
#define DBG_GET_FILE 0

#include "CansatInterface.h"

RT_CanCommander::RT_CanCommander(unsigned long int theTimeOutInMsec):
	RF_Stream(NULL),
	sd(NULL),
	cmdModeTimeOut(theTimeOutInMsec),
	inactivityDuration(0),
	state(State_t::Acquisition),
	acqProcess(NULL)
{}


#ifdef RF_ACTIVATE_API_MODE
void RT_CanCommander::begin(CansatXBeeClient &xbeeClient, SdFat* theSd, AcquisitionProcess* theProcess) {
  RF_Stream = &xbeeClient;
  sd = theSd;
  acqProcess = theProcess;
}
#else
void RT_CanCommander::begin (Stream& theResponseStream, SdFat* theSd, AcquisitionProcess* theProcess) {
  RF_Stream = &theResponseStream;
  sd = theSd;
  acqProcess = theProcess;
}
#endif

void RT_CanCommander::processCmdRequest(const char * request) {
	DASSERT(RF_Stream);
	DPRINTS(DBG_READ_MSG, "processMsg:  received ");
	DPRINTLN(DBG_READ_MSG, request);

	static constexpr byte CmdBufferSize = 100;
	if (strlen(request) >= CmdBufferSize - 1) {
		DPRINTSLN(DBG_DIAGNOSTIC,
				"RT_CanCommander::processRequest: Command too large (ignored).");
		return;
	}

	// Processing the request will modify it. To honor the const-ness of cmd,
	// we work on a copy.
	char cmdCopy[CmdBufferSize];
	strcpy(cmdCopy, request);
	char* nextCharAddress = cmdCopy;
	// Remove final '\n' if any
	while ((cmdCopy[strlen(cmdCopy) - 1] == '\n')
			|| (cmdCopy[strlen(cmdCopy) - 1] == '\r')) {
		cmdCopy[strlen(cmdCopy) - 1] = '\0';
		DPRINTS(DBG_READ_MSG, "Dropped final '\\n' or '\\r'. Msg:");
		DPRINTLN(DBG_READ_MSG, cmdCopy);
	}

	inactivityDuration = 0; // Reset timer first (some commands causes an early return from the function
	long int intRequestType;
	CansatCmdRequestType requestType;
	if (!getMandatoryParameter(nextCharAddress, intRequestType,
			"Missing request type")) {
		return;
	} else {
		requestType= (CansatCmdRequestType) intRequestType;
	}

	DPRINTS(DBG_READ_MSG, "Request type: ");
	DPRINTLN(DBG_READ_MSG, intRequestType);
	switch (requestType) {
	case CansatCmdRequestType::InitiateCmdMode:
		if (state == State_t::Acquisition) {
			state = State_t::Command;
			DPRINTSLN(DBG_READ_MSG, "Switching to cmd mode");
		}
		RF_OPEN_CMD_RESPONSE(RF_Stream)
		;
		*RF_Stream << (int) CansatCmdResponseType::CmdModeInitiated
				<< ",Command mode initiated" ;
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
		return;
		break;
	case CansatCmdRequestType::TerminateCmdMode:
		state = State_t::Acquisition;
		DPRINTSLN(DBG_READ_MSG, "Switching to acquisition mode");
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) CansatCmdResponseType::CmdModeTerminated
				<< ",Command mode terminated";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
		break;
	default:
		break;
	}
	if (state == State_t::Command) {
		bool processed = processStandardCommand(requestType, nextCharAddress);
		if (!processed) {
			processed = processProjectCommand(requestType, nextCharAddress);
		}
		// We return a pointer to our own buffer
		if (!processed) {
			RF_OPEN_CMD_RESPONSE(RF_Stream);
			*RF_Stream << (int) CansatCmdResponseType::UnsupportedRequestType
					<< "," <<  intRequestType << ",Unknown Command.";
			RF_CLOSE_CMD_RESPONSE(RF_Stream);
		}
	}
	inactivityDuration = 0; // reset timer again in case the command processing was long.
	checkTimeout();
	DPRINTS(DBG_READ_MSG, "processMsg over. msg= ");
	DPRINTLN(DBG_READ_MSG, request);
}

bool RT_CanCommander::getParameter(char* &nextCharAddress, const char* &value, const char* defaultValue) {
	DPRINTS(DBG_GET_PARAM, "getParameter (char*). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	if (*nextCharAddress==',') nextCharAddress++; // Skip comma (if any)
	if (*nextCharAddress=='\0') {
		value=defaultValue; // just copy the pointer.
		return false;
	}

    char* separator=strchr(nextCharAddress, ',');
    if (separator) {
    	// Found a separator, there is something behind the parameter.
    	value=nextCharAddress;
    	nextCharAddress=separator+1;
    	*separator='\0';
    } else {
    	value=nextCharAddress;
    	// move nextCharAddress pointer to the final '\0'
    	nextCharAddress+=strlen(nextCharAddress);
	}
    return true;
}

bool RT_CanCommander::getParameter(char* &nextCharAddress, byte& value, byte defaultValue) {
	long aLong;
	bool found=getParameter(nextCharAddress, aLong, (long) defaultValue);
	// default value is set by getParameter();
	value = (byte) aLong;
	return found;
}

bool RT_CanCommander::getParameter(char* &nextCharAddress, long int& value, long int defaultValue) {
	DPRINTS(DBG_GET_PARAM, "getParameter (long). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	bool parameterFound=false;
	const char* stringValue;
	if (getParameter(nextCharAddress, stringValue))
	{
		char * tailPtr;
		DPRINTS(DBG_GET_PARAM, "Parsing long:");
		DPRINT(DBG_GET_PARAM, stringValue);
		DPRINTS(DBG_GET_PARAM, ", nextCharAddress is now:");
		DPRINTLN(DBG_GET_PARAM, nextCharAddress);
		value = strtol(stringValue, &tailPtr, 10);
		if  (tailPtr == stringValue) {
			DPRINTSLN(DBG_GET_PARAM, "tailPtr = stringValue");
			value=defaultValue; // No conversion occurred
		} else {
			parameterFound = true;
		}
	} else {
		value=defaultValue; // Parameter not present.
	}
	return parameterFound;
}

bool RT_CanCommander::getParameter(char* &nextCharAddress, uint32_t& value, uint32_t defaultValue) {
	DPRINTS(DBG_GET_PARAM, "getParameter (uint32_t). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	bool parameterFound=false;
	const char* stringValue;
	if (getParameter(nextCharAddress, stringValue))
	{
		char * tailPtr;
		DPRINTS(DBG_GET_PARAM, "Parsing uint32_t:");
		DPRINT(DBG_GET_PARAM, stringValue);
		DPRINTS(DBG_GET_PARAM, ", nextCharAddress is now:");
		DPRINTLN(DBG_GET_PARAM, nextCharAddress);
		value = strtoul(stringValue, &tailPtr, 10);
		if  (tailPtr == stringValue) {
			DPRINTSLN(DBG_GET_PARAM, "tailPtr = stringValue");
			value=defaultValue; // No conversion occurred
		} else {
			parameterFound = true;
		}
	} else {
		value=defaultValue; // Parameter not present.
	}
	return parameterFound;
}

bool RT_CanCommander::getMandatoryParameter(char* &nextCharAddress,const char* &value, const char* errorMsg) {
	DPRINTS(DBG_GET_PARAM, "getMandatoryParameter (char*). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	if (!getParameter(nextCharAddress, value)) {
		DPRINTSLN(DBG_READ_MSG, "Premature end of command");
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) CansatCmdResponseType::MalformedRequest << "," << errorMsg;
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
		return false;
	}
	return true;
}

bool RT_CanCommander::getMandatoryParameter(char* &nextCharAddress, byte& value, const char* errorMsg) {
	long aLong;
	bool result=getMandatoryParameter(nextCharAddress, aLong, errorMsg);
	if (result) {
		value=(byte) aLong;
	}
	return result;
}

bool RT_CanCommander::getMandatoryParameter(char* &nextCharAddress, long int& value, const char* errorMsg) {
	DPRINTS(DBG_GET_PARAM, "getMandatoryParameter (long). Received:");
	DPRINTLN(DBG_GET_PARAM, nextCharAddress);

	if (!getParameter(nextCharAddress, value)) {
		DPRINTSLN(DBG_READ_MSG, "Premature end of command");
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int)CansatCmdResponseType::MalformedRequest << "," << errorMsg;
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
		return false;
	}
	return true;
}

void RT_CanCommander::checkTimeout() {
  DASSERT(RF_Stream);
  if ((state == State_t::Command) && (inactivityDuration >= cmdModeTimeOut)) {
    state = State_t::Acquisition;
    DPRINTS(DBG_READ_MSG, "timeout elapsed. Inactivity (msec)=");
    DPRINTLN(DBG_READ_MSG, inactivityDuration);
    RF_OPEN_CMD_RESPONSE(RF_Stream);
    *RF_Stream << (int)CansatCmdResponseType::CmdModeTerminated
    		   << ",Acquisition mode resumed";
    RF_CLOSE_CMD_RESPONSE(RF_Stream);
  }
}

bool RT_CanCommander::processStandardCommand(CansatCmdRequestType requestType, char* cmd) {
	bool result=true;
	switch (requestType) {
		    case CansatCmdRequestType::InvalidRequestType: // This is the value obtained if the strtol() conversion failed (not a number, no request type provided...
		      RF_OPEN_CMD_RESPONSE(RF_Stream);
			  *RF_Stream << (int)CansatCmdResponseType::MalformedRequest
				         << ",Missing/Invalid request type";
			  RF_CLOSE_CMD_RESPONSE(RF_Stream);
			  break;
	        case CansatCmdRequestType::DigitalWrite:
	          processReq_DigitalWrite(cmd);
	          break;

	        case CansatCmdRequestType::ListFiles:
	          processReq_ListFiles(cmd);
	          break;

	        case CansatCmdRequestType::GetFile:
	          processReq_GetFile(cmd);
	          break;

	        case CansatCmdRequestType::InitiateCmdMode:
	        case CansatCmdRequestType::TerminateCmdMode:
	          // Two commands already handled.
	          break;

	        case CansatCmdRequestType::GetCampaignStatus:
	          processReq_GetCampaignStatus(cmd);
	          break;

	        case CansatCmdRequestType::StartCampaign:
	          processReq_StartCampaign(cmd);
	          break;

	        case CansatCmdRequestType::StopCampaign:
	          processReq_StopCampaign(cmd);
	          break;

	        case CansatCmdRequestType::PrepareShutdown:
	          processReq_PrepareShutdown(cmd);
	          break;

	        case CansatCmdRequestType::CancelShutdown:
	          processReq_CancelShutdown(cmd);
	          break;

	        default:
	          // Do not report error here: the command could be handled later.
	          result=false;
	} // Switch
	return result;
}

void RT_CanCommander::processReq_GetCampaignStatus(char* & /*nextcharAddress */) {
	if (acqProcess) {
		bool started=acqProcess->measurementCampaignStarted();
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) (CansatCmdResponseType::CampaignStatus) << ',' << (int) started
				<< (started ? ",Campaign started" : ",Campaign not started");
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	} else {
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) (CansatCmdResponseType::UnsupportedRequestType) << ','
				   << (int) (CansatCmdRequestType::StartCampaign) << ",No known acquisition process";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	}
}

void RT_CanCommander::processReq_StartCampaign(char* & /*nextcharAddress */) {
	if (acqProcess) {
		acqProcess->startMeasurementCampaign("Manual command:");
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) (CansatCmdResponseType::CampaignStatus) << ',' << 1
				<< ",Campaign started";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	} else {
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) (CansatCmdResponseType::UnsupportedRequestType) << ','
				   << (int) (CansatCmdRequestType::StartCampaign) << ",No known acquisition process";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	}
}

void RT_CanCommander::processReq_StopCampaign(char* & /*nextcharAddress */) {
	if (acqProcess) {
		acqProcess->stopMeasurementCampaign();
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream 	<< (int) (CansatCmdResponseType::CampaignStatus) << ',' << 0
				<< ",Campaign stopped";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	} else {
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) (CansatCmdResponseType::UnsupportedRequestType) << ','
				   << (int) (CansatCmdRequestType::StopCampaign) << ",No known acquisition process";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	}
}

void RT_CanCommander::processReq_DigitalWrite(char* &nextCharAddress) {
	byte pinNbr, newState;
    if (!getMandatoryParameter(nextCharAddress, pinNbr, "Missing pin number")) return;
    DPRINTS(DBG_READ_MSG, "Digital write: pin# ");
    DPRINTLN(DBG_READ_MSG, pinNbr);
    if (!getMandatoryParameter(nextCharAddress, newState, "Missing pin state")) return;
    DPRINTS(DBG_READ_MSG, " new state =  ");
    DPRINTLN(DBG_READ_MSG, newState);
    digitalWrite(pinNbr, newState);
    RF_OPEN_CMD_RESPONSE(RF_Stream);
    *RF_Stream << (int)CansatCmdResponseType::DigitalWriteOK << ',' << pinNbr << ','
  		     << newState << ",pin#" << pinNbr << " now " << (newState? "HIGH":"LOW");
    RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void RT_CanCommander::processReq_PrepareShutdown(char* & /* nextCharAddress*/ ) {
	 bool success=(preparePrimaryMissionForShutdown() &&
			prepareSecondaryMissionForShutdown());
	 RF_OPEN_CMD_RESPONSE(RF_Stream);
	 if (success) {
	     *RF_Stream << (int)CansatCmdResponseType::ReadyForShutdown
	    		<< ",Ready for shutdown (after possible mechanical move completion)";
	 } else {
	     *RF_Stream << (int)CansatCmdResponseType::ShutdownPreparationFailed << ','
	    		<< ",Could not prepare for shutdown";
	 }
	 RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

void RT_CanCommander::processReq_CancelShutdown(char* & /* nextCharAddress */) {
	 bool success=(cancelPrimaryMissionShutdown() &&
			cancelSecondaryMissionShutdown());
	 RF_OPEN_CMD_RESPONSE(RF_Stream);
	 if (success) {
	     *RF_Stream << (int)CansatCmdResponseType::ShutdownCancelled
	    		<< ",Shutdown cancelled (after possible mechanical move completion)";
	 } else {
	     *RF_Stream << (int)CansatCmdResponseType::ShutdownCancellationFailed << ','
	    		<< ",Could not prepare for shutdown";
	 }
	 RF_CLOSE_CMD_RESPONSE(RF_Stream);
}

bool RT_CanCommander::checkSdAvailable(CansatCmdRequestType cmd) const {
	if (!sd) {
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) CansatCmdResponseType::UnsupportedRequestType << ','
				<< (int) cmd << ",no SdFat object.";
		RF_CLOSE_CMD_RESPONSE(RF_Stream)
		return false;
	} else return true;
}

void RT_CanCommander::processReq_ListFiles(char* &nextCharAddress) {
	const char *path;
	if (!getMandatoryParameter(nextCharAddress, path, "Missing path")) return;
	uint32_t firstFile;
	uint32_t numFiles, lastFile;
	if (getParameter(nextCharAddress, firstFile, 1)) {
		getParameter(nextCharAddress, numFiles, 1000);
	} else
		numFiles = 1000; // Never list more than 1000 files.
    lastFile=firstFile+numFiles-1;

	DPRINTS(DBG_READ_MSG, "List files, dir=");
	DPRINT(DBG_READ_MSG, path);
	DPRINTS(DBG_READ_MSG, ",");
	DPRINT(DBG_READ_MSG, numFiles);
	DPRINTS(DBG_READ_MSG, " files from file ");
	DPRINTLN(DBG_READ_MSG, firstFile);

	if (!checkSdAvailable(CansatCmdRequestType::ListFiles)) return;


	File dir = sd->open(path, FILE_READ);
	if (dir.isDir()) {
		DPRINTSLN(DBG_READ_MSG, "listing Files on the SD Card");
		unsigned int currentFile=1;
		bool complete=true;
		while (1) {
			File entry =  dir.openNextFile();
			constexpr int FileNameLength=25;
			char fileName[FileNameLength];
			if (! entry) {
				// no more files
				break;
			}
			if (currentFile > lastFile) {
				complete=false;
				break;
			}
			if (currentFile >= firstFile) {
				entry.getName(fileName, FileNameLength);
				RF_OPEN_CMD_RESPONSE(RF_Stream);
				*RF_Stream << (int) CansatCmdResponseType::FileListEntry << ','
						<< currentFile << ',' << fileName;
				if (entry.isDirectory()) {
					*RF_Stream << '/';
				} else {
					// files have sizes, directories do not
					*RF_Stream << ',' << entry.size();
				}
				// Close with a small delay to wait for the ack: since we are possibly sending a large number
				// of frames, not reading the acks will cause the internal queue of the XBee modules to
				// be saturated, and generate harmless but inelegant error messages.
				RF_CLOSE_CMD_RESPONSE_WITH_ACK(RF_Stream,100);
				entry.close();
				delay(100); // Avoid overflowing the radio channel
			}
			currentFile++;
		}
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int)CansatCmdResponseType::FileListComplete
				   << ',' << path << ',' << (currentFile >= firstFile ? currentFile - firstFile : 0) ;
		if (complete) {
			*RF_Stream << ",1, no file left to list";
		}
		else {
			*RF_Stream << ",0, more files to list";
		}
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	} else {
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int)CansatCmdResponseType::InvalidPath
				   << ',' << path <<  ",Inexistent Directory.";
		RF_CLOSE_CMD_RESPONSE(RF_Stream)
	}
	dir.close();
}

void RT_CanCommander::processReq_GetFile(char* &nextCharAddress) {

const char* path;
uint32_t startByte, numBytes;
if (!getMandatoryParameter(nextCharAddress, path, "Missing path")){
	return;
}
DPRINTS(DBG_GET_FILE, "Get file, path=");
DPRINTLN(DBG_GET_FILE, path);
if (getParameter(nextCharAddress, startByte, 0)) {
	getParameter(nextCharAddress, numBytes, 1000);
} else numBytes = 1000;

if (!checkSdAvailable(CansatCmdRequestType::GetFile)) return;

File file = sd->open(path, FILE_READ);
if (!file) {
	DPRINTSLN(DBG_READ_MSG, "Error Opening File");
	RF_OPEN_CMD_RESPONSE(RF_Stream);
	*RF_Stream << (int) CansatCmdResponseType::InvalidPath << ',' << path
			<< ",Cannot open file (inexistent?)";
	RF_CLOSE_CMD_RESPONSE(RF_Stream);
	return;
}
if (!file.isFile()) {
	DPRINTSLN(DBG_READ_MSG, "Path is not a file");
	RF_OPEN_CMD_RESPONSE(RF_Stream);
	*RF_Stream << (int) CansatCmdResponseType::InvalidPath << ',' << path
			<< ",Not a regular file";
	RF_CLOSE_CMD_RESPONSE(RF_Stream);
	file.close();
	return;
}

#ifdef RF_ACTIVATE_API_MODE
// This version is more efficient, but does not work in transparent mode:
// Blocks including an end-of-line are not received.
constexpr byte ReadBufferSize=CansatXBeeClient::MaxPayloadSize; // File is sliced in blocks of n chars
constexpr unsigned long DelayBetweenBlocks= 110;
// Xbee in API mode: do not exceed MaxPayloadSize:
// 95 is not enough, 100 does not prevent all errors.
// 100 is enough.
#else
constexpr byte ReadBufferSize = 150; // File is sliced in blocks of n chars (Typical: 150)
constexpr unsigned long DelayBetweenBlocks = 30;
// Xbee in Transparent mode, with buffer of 150 chars:
// 20 is not enough,
// 30 is enough.
#endif

DPRINTS(DBG_GET_FILE, "Processing request for file: ");
DPRINTLN(DBG_GET_FILE, path);
if (!file.seek(startByte)) {
	// Cannot find start position
	RF_OPEN_CMD_RESPONSE(RF_Stream);
	*RF_Stream << (int) CansatCmdResponseType::MalformedRequest << ',' << path
			<< ",Cannot find position " << startByte << " in file" ;
	file.close();
	RF_CLOSE_CMD_RESPONSE(RF_Stream);
	return;
}
RF_OPEN_CMD_RESPONSE(RF_Stream);
*RF_Stream << (int) CansatCmdResponseType::FileContent << ',' << path
		<< ",content (from byte " << startByte << ", by "
		<< ReadBufferSize << " max)";
RF_CLOSE_CMD_RESPONSE(RF_Stream);
RF_OPEN_CMD_RESPONSE(RF_Stream);
*RF_Stream << (int) CansatCmdResponseType::BeginFileContent << ",--------";
RF_CLOSE_CMD_RESPONSE(RF_Stream);
delay(50);

bool error = false;
unsigned long counter = 0;
DPRINTS(DBG_GET_FILE, "Sending in blocks of ");
DPRINT(DBG_GET_FILE, ReadBufferSize);
DPRINTSLN(DBG_GET_FILE, " bytes");

#ifdef RF_ACTIVATE_API_MODE
// This version is more efficient, but does not work in transparent mode:
// Blocks including an end-of-line are not received.
uint8_t buffer[ReadBufferSize];
buffer[0]=(uint8_t) CansatFrameType::StringPart;
uint8_t seqNbr=0;
while (file.available() && (counter < numBytes)) {
	auto read=file.read(buffer+2, ReadBufferSize-3);
	if (read < 0) {
		error = true;
		DPRINTS(DBG_GET_FILE, "***Error reading file");
		break;
	}
	if (counter+read >= numBytes) {
		read=numBytes-counter;
	}
	counter+=read;
	buffer[read+2]='\0';
	DPRINTS(DBG_GET_FILE, "Read ");
	DPRINT(DBG_GET_FILE, read);
	DPRINTS(DBG_GET_FILE, " bytes, counter=");
	DPRINT(DBG_GET_FILE, counter);
	DPRINTS(DBG_GET_FILE, ", seqNbr=");
	DPRINTLN(DBG_GET_FILE, seqNbr);
	buffer[1]=seqNbr;
	RF_Stream->XBeeClient::send((uint8_t*) buffer, (uint8_t) (read+3), 0);
	// Mystery: why does the above line not compile when written as
	//          RF_Stream->send((uint8_t*) buffer, (uint8_t) (read+3), 0);
	delay(DelayBetweenBlocks);
	seqNbr++;
}
#else

/* About \n characters:
 * -------------------
 * In transparent mode, end-of-line characters trigger the transmission.
 * As a consequence, a block cannot be transmitted with a \n in the middle.
 * Hence:
 * 	1. Whenever an EOL is encountered in the file, the block is sent immediately, with an additional EOL
 * 	2. Whenever the buffer is full, it is streamed, without an additional EOL
 * 	3. When the EOF is encountered, the last chunk is sent with an additional string terminator and EOL.
 * Beware that counter is the index of the character. Index 0 is the first character read, .
 */
int blockCounter = 0;
bool sendBlock = false;
bool addEndLine;
char buffer[ReadBufferSize];
elapsedMillis timeSinceLastEmission = 10000;
while (file.available() && (counter < (unsigned long) numBytes)) {
	int i = file.read();
	if (i < 0) {
		error = true;
		break;
	} else {
		char c = (char) i;
		if (c == '\r') {
			// Just ignore: EOL is '\n\r'. Only react on \n.
			// NB: counter is increased anyway (the character is read)
			//     but blockCounter is not (it is not transmitted).
			counter++;
			continue;
		}
		if ((c == '\n') || (c == '\0')) {
			if (blockCounter > 0) {
				// Do not transmit CR or \0 as first character: the previous
				// block ended with CR. We do not transmit 2 successive CR.
				buffer[blockCounter] = '\0';
				sendBlock = true;
				addEndLine = (c == '\n');
				blockCounter++;
			}
		} else {
			buffer[blockCounter] = c;
			blockCounter++;
		}
	}

	// If the buffer is one but full (ie blockCounter = bufferSize-1): and '\0' and send it, no final EOL.
	if (blockCounter == (ReadBufferSize - 1)) {
		buffer[blockCounter] = '\0';
		sendBlock = true;
		addEndLine = false;
	} else {
		// At this stage, we could have reached the end of file or reached the limit of numBytes
		// with a non-empty, buffer
		// In this case, send it with final EOL
		if ((!file.available() || (counter == (unsigned long) numBytes - 1))
				&& (blockCounter > 0))
		{
			buffer[blockCounter] = '\0';
			sendBlock = true;
			addEndLine = true;
		}
	}

	if (sendBlock) {
		while (timeSinceLastEmission < DelayBetweenBlocks) {
			delay(10);
		}
		// NB: In transparent mode, nothing is sent before an ENDL is sent!
		*RF_Stream << (const char*) buffer;
		if (addEndLine) {
			*RF_Stream << ENDL;
			timeSinceLastEmission = 0;
		}
		blockCounter = 0;
		sendBlock = false;
	}
	counter++;
}
#endif

	if (error) {
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << ENDL;
		*RF_Stream << (int) CansatCmdResponseType::FileError
				<< ",Error reading file";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	} else {
		DPRINTSLN(DBG_GET_FILE, "Closing transmission");
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) CansatCmdResponseType::EndFileContent
				<< ",=========";
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
		RF_OPEN_CMD_RESPONSE(RF_Stream);
		*RF_Stream << (int) CansatCmdResponseType::FileContent << ',' << path
				<< ',' << startByte << ',' << numBytes
				<< (file.available() ? ",0, EOF not reached" : ",1, EOF reached");
		RF_CLOSE_CMD_RESPONSE(RF_Stream);
	}
	file.close();
}  // getFile()

