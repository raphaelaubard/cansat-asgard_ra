#pragma once
#include <Servo.h>
#include "Arduino.h"
//Création de la classe ServoLatch
class ServoLatch {
  public :
    //Configuration de la Pin utilisée par une fonction bouléenne
    bool begin(const uint8_t PWM_Pin);
    //Création de la fonction UnLock qui bouge l'hélice du Servo de 90° puis coupe le courant éléctrique
    void unlock();
    //Création de la fonction Lock qui remet l'hélice à sa position initiale (0°) puis coupe le courant éléctrique
    void lock();

  private :

    Servo myServo;
    unsigned char thePWM_Pin;
    const uint8_t UnLockPosition = 90;
    const uint8_t LockPosition = 0;
};
