/*
   XBeeClient.h
*/

#pragma once
#include "CansatConfig.h" // for RF_ACTIVATE_API_MODE

#ifdef RF_ACTIVATE_API_MODE

#include "Arduino.h"
#include <XBee.h>
#include "StringStream.h"

/**
 * @ingroup CSPU_XBee
 * @brief Interface class to transport data between XBee modules in API mode.
 * It allows for sending and receving binary payloads (methods send and receive)
 * and for sending strings (methods openStringMsg, operator << and closeStringMsg.
 * Strings are transported as binary payloads including
 * 	- A type identifier (1 byte)
 * 	- 0xFF as second byte.
 * 	- The ascii codes of the characters.
 * 	- A final \ 0 delimiter
 * Detecting string messages among received payloads are the user responsibility.
 *
 * Debugging: incoming and/or outgoing frames are displayed on Serial if protected
 *     	      constants displayOutgoingFramesOnSerial and/or
 *     	      displayIncomingFramesOnSerial are true
 *
 */
class XBeeClient {
  public:
    /**Build up constructor - ATTENTION, values must be given in hexa (ex: 0x0013a200)
      @param SH in the Serial Number High specific to the receiving Xbee
      @param SL is the Serial Number Low specific to the receiving Xbee
    */
    XBeeClient(uint32_t SH, uint32_t SL);
    virtual ~XBeeClient() {};

    /** Initialize point to point radio link
        @param stream The serial object to use to communicate with
                the XBee module (can be a HardwareSerial or a SoftwareSerial. */
    virtual void begin(Stream &stream);

    /** Send data (after begin() has been called)
        @param data The payload to transfer
        @param dataSize The number of bytes of the payload
        @param timeOutCheckResponse The maximum delay to wait for a reception
        				acknowledgment by the destination XBee (msec). If 0,
        				the reception ack will not be checked.
        @param SH The destination address (high word)
        @param SL The destination address (low word).  If SH=SL=0, the default
        			   address provided in the constructor is used.
        @return True if transfer successful, false otherwise.
    */
    virtual bool send(uint8_t* data,
                      uint8_t dataSize,
                      int timeOutCheckResponse = 300,
                      uint32_t SH = 0,
                      uint32_t SL = 0);

    /** Check whether data is received, the user should then print data using a for{} loop
     *  (after begin() has been called).
        @param payload If anything is received, this pointer is set
                 to an internal buffer containing the received
                 payload. Ownership of this buffer remains in the object.
        @param payloadSize If anything is received, this parameters is
                 set to the number of bytes of the received payload.
        @return True if anything received, false otherwise
    */
    virtual bool receive(uint8_t* &payload, uint8_t& payloadSize);

    /** Start a string message. This method should be called before streaming the
     *  string to be transported using operator<< (and after begin() has been called).
     *  @param type A type identifier, transported as first byte of the payload.
     *  			(string will start at byte 3.
     *  @param seqNbr A number that can be used to define a sequence in the strings
     *  	   to allow the receiver to check that no message was lost. It is
     *  	   transported as byte 2 in the payload.
     */
    virtual void openStringMessage(uint8_t type, uint8_t seqNbr=0xFF);

    /** Terminate and send a string message. This method must be used after a call
     *  to openStringMessage and the streaming of the string itself using operator<<.
     *  It adds the final \0 delimiter and sends the string.
     */
    virtual bool closeStringMessage(int timeOutCheckResponse = 300);

   	/** Print the content of the payload in human-readable format on the Serial interface
   	 *  (for debugging only).
   	 *  @param data Payload pointer
   	 *  @param dataSize Payload size
   	 */
    void displayFrame(uint8_t* data, uint8_t dataSize) const;

    /** Extract data from the payload to fill a string buffer.
 	* @param string The buffer to fill with the null-terminated string.
 	* 			 allocated by the caller, must be at least dataSize bytes.
 	* @param stringType The frame type found in the payload.
 	* @param seqNbr The optional sequence number associated with the string.
 	* @param data Payload pointer
 	* @param dataSize Payload size
 	* @pre The payload is expected to be exactly a byte with value
 	* 		followed by an unused byte and a null-terminated string.
 	* @return True if operation was successful, false otherwise.
 	*/
    bool getString(char* string, uint8_t &stringType, uint8_t& seqNbr,
    		const uint8_t* data,uint8_t dataSize) const;

   	/** Print the content of the payload when containing a string
   	 *  (for debugging only).
   	 *  @param data Payload pointer
   	 *  @param dataSize Payload size
   	 */
    virtual void displayString(uint8_t* data, uint8_t dataSize) const;

    /** The maximum number of bytes that can be transported in a single call to send() */
    static constexpr uint8_t MaxPayloadSize=MAX_FRAME_DATA_SIZE-16;
    /** The maximum number of characters that can be transported in a single string */
    static constexpr uint8_t MaxStringSize=MaxPayloadSize-3;
  protected:
    static constexpr bool DisplayOutgoingFramesOnSerial = false; /**< Activate for debugging only */
    static constexpr bool DisplayIncomingFramesOnSerial = false; /**< Activate for debugging only */

  private:
    XBee xbee;
    XBeeAddress64 defaultReceiverAddress;
    String msgBuffer; /**<The internal buffer to build the string messages */
    StringStream sstr; /**< A stream to fill the buffer */
    template<class T> friend XBeeClient &operator <<(XBeeClient &xb, T arg);
};

template<class T> inline XBeeClient &operator <<(XBeeClient &xb, T arg) {
  xb.sstr << arg; return xb;
}

#endif
