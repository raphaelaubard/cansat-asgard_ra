/*
 * Test the serial stream macroes. See limitations in SerialStream.h.
 * 
 */
#include "SerialStream.h"
//#define TEST_BASE_TYPES

void setup() {
  Serial.begin(9600);
  while (!Serial) ;

  Serial.println(F("Testing streaming to object serial"));
#ifdef TEST_BASE_TYPES
  Serial.println(F("=== Testing with base types.... (undefine TEST_BASE_TYPES to test other types) ==="));
  Serial vv("Sending byte with value 113: ") << (byte) 113 << ENDL;
  Serial VV("Sending unsigned int with value 113: ") << (int) 113 << ENDL;
  Serial vv("Sending  int with value 113: ") << (unsigned int) 113 << ENDL;
  Serial vv("Sending  long int  with value 113: ") << (long int) 113L << ENDL;
  Serial vv("Sending  float with value 113: ") <<  113.0 << ENDL;
  Serial vv("Sending  double with value 113: ") <<  (double) 113.0 << ENDL;
  Serial vv("Check no dynamic memory is used besides the unavoidable 188 bytes consumed when using Serial ") << ENDL;
#else
  String str("This is the content of a String object");
   Serial.println(F("=== Testing with non-base types.... (define TEST_BASE_TYPES to test base types) ==="));
   Serial vv("Sending a String: ") << str << ENDL;
#endif
  Serial.println(F("Done"));
}

void loop() {
  // put your main code here, to run repeatedly:

}
