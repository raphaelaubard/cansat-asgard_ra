// FlashLogger.cpp

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "FlashLogger.h"

#define USE_ASSERTIONS
//#define USE_TIMER

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "Timer.h"

#define DBG_LOG 0
#define DBG_INIT_FLASH_LOGGER 0
#define DBG_FREE_SPACE 0
#define DBG_DIAGNOSTIC 1

bool FlashLogger::storageInitialized = false;

// Configuration of the flash chip pins and flash fatfs object.
// You don't normally need to change these if using a Feather/Metro
// M0 express board.
#define FLASH_TYPE     SPIFLASHTYPE_W25Q16BV  // Flash chip type.
// If you change this be
// sure to change the fatfs
// object type below to match.

FlashLogger::FlashLogger() : flash(SS1, &SPI1), fatfs(flash), instanceInitialized(false) {}
FlashLogger::~FlashLogger() {
  if (instanceInitialized) storageInitialized = false;
}

bool FlashLogger::initStorage() {
  DPRINTS(DBG_INIT_FLASH_LOGGER, "storageInitialized=");
  DPRINT(DBG_INIT_FLASH_LOGGER, storageInitialized);
  DPRINTS(DBG_INIT_FLASH_LOGGER, ", instanceInit=");
  DPRINTLN(DBG_INIT_FLASH_LOGGER, instanceInitialized);
  if (instanceInitialized) return true;

   if (storageInitialized) {
    DPRINTSLN(DBG_DIAGNOSTIC, "Error: initializing more than 1 FlashLogger instance");
    DASSERT(!storageInitialized);
  }
  bool resultFlash = flash.begin(FLASH_TYPE);
  DPRINTS(DBG_INIT_FLASH_LOGGER, "flash init: ");
  DPRINTLN(DBG_INIT_FLASH_LOGGER,  resultFlash );
  bool resultFatfs;
  if (resultFlash) {
    DFREE_RAM(DBG_INIT_FLASH_LOGGER);
    resultFatfs = fatfs.begin();
    DPRINTS(DBG_INIT_FLASH_LOGGER, "fatfs init: ");
    DPRINTLN(DBG_INIT_FLASH_LOGGER, resultFatfs);
  }
  Serial << "end of initStorage" << ENDL;
  instanceInitialized = storageInitialized = resultFlash;
  
  DPRINTS(DBG_INIT_FLASH_LOGGER, "storageInitialized=");
  DPRINT(DBG_INIT_FLASH_LOGGER, storageInitialized);
  DPRINTS(DBG_INIT_FLASH_LOGGER, ", instanceInit=");
  DPRINTLN(DBG_INIT_FLASH_LOGGER, instanceInitialized);
  return ( resultFlash && resultFatfs);
}

bool FlashLogger::log(const String& data, const bool addFinalCR) {
  DBG_TIMER("FlashLogger::log");
  bool result = false;
  FlashFile dataFile = fatfs.open(fileName(), FILE_WRITE);

  if (dataFile) {
    if (addFinalCR) {
      dataFile.println(data.c_str());
    }
    else {
      dataFile.print(data.c_str());
    }
    dataFile.close();

    DPRINTS(DBG_LOG, "data written: ");
    DPRINTLN(DBG_LOG, data);
    result = true;
  }
  else {
    DPRINTSLN(DBG_DIAGNOSTIC, "error opening file");
  }
  return result;
}

float FlashLogger::getFreeSpaceInMBytes()
{
  DBG_TIMER("logger::freeSpaceInMBytes");
  DPRINTSLN(DBG_FREE_SPACE, "Checking free space on Flash memory file system...");

  FATFS* fatfs;
  DWORD freeClusters;
  FRESULT res = f_getfree("0:/", &freeClusters, &fatfs); // fatfs.vol()->freeClusterCount();
  if (res) {
    DPRINTS(DBG_DIAGNOSTIC, "f_getfree() returned ");
    DPRINTLN(DBG_DIAGNOSTIC, res);
    return 0L;
  }
  DPRINTS(DBG_FREE_SPACE, "Free clusters:");
  DPRINTLN(DBG_FREE_SPACE, freeClusters);

  float freeMB = freeClusters * fatfs->csize; // csize= Cluster size (in sectors).
  DPRINTS(DBG_FREE_SPACE, "Free sectors:");
  DPRINTLN(DBG_FREE_SPACE, freeMB);

  // Sector is always 512 bytes, so there are 2*1024 sectors/MByte.
  freeMB /= (2 * 1024);
  DPRINTS(DBG_FREE_SPACE, "Free space MB: ");
  DPRINTLN(DBG_FREE_SPACE, freeMB);
  return freeMB;
}

unsigned long FlashLogger::fileSize() {
  if (!fileExists(fileName().c_str())) return 0;
  FlashFile theFile = fatfs.open(fileName(), FILE_READ);
  uint32_t size = theFile.size();
  theFile.close();
  return size;
}

#endif // board selection 
