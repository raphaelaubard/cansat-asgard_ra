#include <Wire.h>
#include <Adafruit_LSM9DS0.h>


// Create sensor instances.
Adafruit_LSM9DS0  lsm;

// This sketch can be used to output raw sensor data in a format that
// can be understood by MagMaster calibration program.
//
// Sketch customized by A. Baudhuin for LSM9DS0.

void setup()
{
  bool error = false;
  Serial.begin(9600);
  // Wait for the Serial Monitor to open (comment out to run without Serial Monitor)
  //while(!Serial);

  Serial.println(F("Adafruit 9 DOF Magnetometer calibration with MagMaster")); Serial.println("");

  // Initialize the sensors.
  if (!lsm.begin())
  {
    Serial.println("Oops ... unable to initialize the LSM9DS0. Check your wiring!");
    error = true;
  } //else Serial.println("LSM9DS0 init ok");

  // 1.) Set the accelerometer range
  lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_2G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_4G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_6G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_8G);
  //lsm.setupAccel(lsm.LSM9DS0_ACCELRANGE_16G);

  // 2.) Set the magnetometer sensitivity
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_2GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_4GAUSS);
  //lsm.setupMag(lsm.LSM9DS0_MAGGAIN_8GAUSS);
  lsm.setupMag(lsm.LSM9DS0_MAGGAIN_12GAUSS);

  // 3.) Setup the gyroscope
  lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_245DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_500DPS);
  //lsm.setupGyro(lsm.LSM9DS0_GYROSCALE_2000DPS);

  if (error) {
    Serial.println("Error during init. Aborted");
    while (1);
  } // else Serial.println("Init OK");
}

void loop(void)
{
  lsm.read();

  Serial.flush(); 
  Serial.print((int) lsm.magData.x); 
  Serial.print(",");
  Serial.print((int)lsm.magData.y);
  Serial.print(",");
  Serial.print((int)lsm.magData.z);
  Serial.println();

  delay(100); 
}
