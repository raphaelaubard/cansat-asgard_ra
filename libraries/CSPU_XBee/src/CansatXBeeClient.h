/*
   CansatXbeeClient.h
*/

#pragma once

#include "XBeeClient.h"
#include "CansatConfig.h" // for RF_ACTIVATE_API_MODE
#include "CansatInterface.h" // Required anyway by the macro's.

#ifdef RF_ACTIVATE_API_MODE
// This symbol is defined in CansatConfig.h. Note that XBeeClient.h should be included anyway:
// It contains some macro's useful in Transparent mode.

#include "CansatRecord.h"

/**
 * @ingroup CSPU_XBee
 * @brief Subclass of XBeeClient adding the capability to transport CansatRecords and
 *        string of predefined types easily (types are defined by enum CansatFrameType
 *        in CansatInterface.h).
 *
 *        Emission of a CansatRecord: use the send() method.
 *        Emission of a string message:
 * 			 @code
 *        	 openStringMsg(CansatFrameType::xxxxxx);
 *        	 myCansatXBeeClient << "my string"
 *        	 closeStringMsg();
 * 			 @endcode
 *        Reception: loop on:
 *           @code
 *           MySubclassOfCansatRecord myRecord;
 *           char	myString[maxStringLength];
 *           CansatFrameType theType;
 *           bool gotRecord;
 *           if (receive(myRecord, myString, theType, gotRecord)) {
 *           	if (gotRecord) {
 *           		 		... handle the record...
 *           	else {
 *           	   ... handle myString according to theType.
 *           	}
 *           }
 *           @endcode
 *
 * Configuration parameters:
 * 	 - Protected constant CansatXBeeClient::displayOutgoingMsgOnSerial and
 * 	   displayIncomingMsgOnSerial control the echo of messages on Serial
 * 	   (should be false in operation).
 */
class CansatXBeeClient : public XBeeClient {
  public:
	CansatXBeeClient(uint32_t SH, uint32_t SL): XBeeClient(SH, SL) {};
    virtual ~CansatXBeeClient() {};
    /**Send a data record (after begin() has been called).
       @param record, The payload to transfer
       @param timeOutCheckResponse The maximum delay to wait for a reception
        				acknowledgment by the destination XBee (msec). If 0,
        				the reception ack will not be checked.

       @return True if transfer is successful, false otherwise
    */
    virtual bool send(const CansatRecord& record,  int timeOutCheckResponse = 0);

    /** Check whether data is received (record or string). This should be used in the
     *  receiver's main loop (after begin() has been called).
     *  @param record The record to be fill with the data (when a record is received)
     *  @param string The buffer to be filled with the null-terminated string (when a
     *  			string is received.
	 * 			 	It is allocated by the caller, must allow for the longest possible
	 * 			 	string transported (which is MaxStringLength+1 byte).
	 * @param stringType The frame type associated with a received string.
	 * @param seqNbr The optional sequence number associated with the string.
	 * @param gotRecord This is set to true when a record is received, false when a
	 *     		    string is received.
	 * @return True if anything received, false otherwise
     */
    virtual bool receive(CansatRecord& record, char* string, CansatFrameType &stringType,
    					uint8_t& seqNbr, bool& gotRecord);

    /** Check whether a string is received (record are discarded). This should be used in the
     *  receiver's main loop (after begin() has been called) whn waiting for strings only.
     *  @param string The buffer to be filled with the null-terminated string (when a
     *  			string is received.
	 * 			 	It is allocated by the caller, must allow for the longest possible
	 * 			 	string transported (which is MaxStringLength+1 byte).
	 * @param stringType The frame type associated with a received string.
 	 * @param seqNbr The optional sequence number associated with the string.
	 * @return True if a valid string was received, false otherwise
     */
    virtual bool receive(char* string, CansatFrameType &stringType, uint8_t& seqNbr	);

    /** Begin a new string message of the provided type. This should be called
     *  whenever starting a new string message (before using the streaming operator
     *  and closing the message with closeStringMessage).
     *  @param recordType The record type set in the frame's first byte.
     *  @param seqNbr A number that can be used to define a sequence in the strings
     *  	   to allow the receiver to check that no message was lost.
    */
    virtual void openStringMessage(CansatFrameType frameType = CansatFrameType::StatusMsg, uint8_t seqNbr=0xFF);

    /** Extract data from the payload to fill a CansatRecord.
       @param record The record to fill with the data
       @param data Payload pointer
       @param dataSize Payload size
       @pre The payload is expected to be exactly a byte with value
      		CansatFrameType::DataRecord, an unused byte and
            a CansatRecord.
       @return True if operation was successful, false otherwise.
    */
    bool getDataRecord(CansatRecord& record, const uint8_t* data, uint8_t dataSize) const;
    
    /** Extract data from the payload to fill an string buffer.
	* @param data The buffer to fill with the null-terminated string.
	* 			 allocated by the caller, must be at least dataSize bytes.
	* @param stringType The frame type found in the payload.
	* @param seqNbr The optional sequence number associated with the string.
	* @param data Payload pointer
	* @param dataSize Payload size
	* @param silentIfError If true, no error message is provided when the
	* 	   provided buffer does not contain a valid string.
	* @pre The payload is expected to be exactly a byte with value
	* 		CansatFrameType::StatusMsg, CmdRequest, CmdResponse or StringPart,
	* 		followed by an unused byte and a null-terminated string.
	* @return True if operation was successful, false otherwise.
	*/
   	bool getString(char* string, CansatFrameType &stringType, uint8_t& seqNbr,
   			const uint8_t* data,uint8_t dataSize, bool silentIfError=false) const;

   	/** Print the content of the payload when containing a string
   	 *  (for debugging only).
   	 *  @param data Payload pointer
   	 *  @param dataSize Payload size
   	 */
    virtual void displayString(uint8_t* data, uint8_t dataSize) const;

  protected:
    static constexpr bool DisplayOutgoingMsgOnSerial = false; /**< Activate for debugging only */
    static constexpr bool DisplayIncomingMsgOnSerial = false; /**< Activate for debugging only */
};

// The following macro's allow for sending strings with the same instructions
// in API and in transparent mode.
#  define GET_RF_STREAM(CansatHardwareScanner) CansatHardwareScanner.getRF_XBee();
#  define RF_OPEN_STRING(object) object->openStringMessage(CansatFrameType::StatusMsg);
#  define RF_OPEN_STRING_PART(object, seqNbr) object->openStringMessage(CansatFrameType::StringPart, seqNbr);
#  define RF_OPEN_CMD_REQUEST(object) object->openStringMessage(CansatFrameType::CmdRequest);
#  define RF_OPEN_CMD_RESPONSE(object) object->openStringMessage(CansatFrameType::CmdResponse);
#  define RF_CLOSE_STRING(object) object->closeStringMessage(0);
#  define RF_CLOSE_STRING_PART(object) object->closeStringMessage(0);
#  define RF_CLOSE_CMD_REQUEST(object) object->closeStringMessage(0);
#  define RF_CLOSE_CMD_RESPONSE(object) object->closeStringMessage(0);
#  define RF_CLOSE_STRING_WITH_ACK(object, delay) object->closeStringMessage(delay);
#  define RF_CLOSE_STRING_PART_WITH_ACK(object, delay) object->closeStringMessage(delay);
#  define RF_CLOSE_CMD_REQUEST_WITH_ACK(object, delay) object->closeStringMessage(delay);
#  define RF_CLOSE_CMD_RESPONSE_WITH_ACK(object, delay) object->closeStringMessage(delay);
#else
#  define GET_RF_STREAM(CansatHardwareScanner) CansatHardwareScanner.getRF_SerialObject();
#  define RF_OPEN_STRING(object) *object << (int) CansatFrameType::StatusMsg <<",";
#  define RF_OPEN_STRING_PART(object, seqNbr) *object << (int) CansatFrameType::StringPart <<",";
#  define RF_OPEN_CMD_REQUEST(object) *object << (int) CansatFrameType::CmdRequest <<",";
#  define RF_OPEN_CMD_RESPONSE(object) *object << (int) CansatFrameType::CmdResponse <<",";
#  define RF_CLOSE_CMD_REQUEST(object) *object << ENDL;
#  define RF_CLOSE_CMD_RESPONSE(object)  *object << ENDL;
#  define RF_CLOSE_STRING(object)  *object << ENDL;
#  define RF_CLOSE_STRING_PART(object)  *object << ENDL;
#  define RF_CLOSE_STRING_WITH_ACK(object, delay) *object << ENDL;
#  define RF_CLOSE_STRING_PART_WITH_ACK(object, delay) *object << ENDL;
#  define RF_CLOSE_CMD_REQUEST_WITH_ACK(object, delay) *object << ENDL;
#  define RF_CLOSE_CMD_RESPONSE_WITH_ACK(object, delay) *object << ENDL;

#endif
