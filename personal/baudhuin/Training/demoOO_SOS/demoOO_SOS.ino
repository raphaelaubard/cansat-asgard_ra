#include "BlinkingLED.h"
#include "SOS.h"
#define DEBUG_CSPU
#include "DebugCSPU.h"

#define DBG_MAIN 1

BlinkingLED led1(8, 2000);
BlinkingLED led2(11,300);
SOS mySOS(LED_BUILTIN);

void setup() {
  // put your setup code here, to run once:
  DINIT(115200);
  DPRINTSLN(DBG_MAIN, "Setup complete");
}

void loop() {
  // put your main code here, to run repeatedly:
  led1.run();
  led2.run();
  mySOS.run();
}
