/*
   TemperatureClient.cpp
*/

#include "TemperatureClient.h"
#include "Arduino.h"

TemperatureClient::TemperatureClient(byte pinNumber) {
  pinNumber_ = pinNumber;
}

void TemperatureClient::begin(float theReferenceVoltage, int theNumADC_Steps) {
  refVoltage = theReferenceVoltage;
  numADC_Steps = theNumADC_Steps;
}


bool TemperatureClient::readData(IsaTwoRecord& record) {
  constexpr float Rdiv = 10000.; /* Known resistor value of the voltage divider */

  int measure = analogRead(pinNumber_); /* Measure of the voltage */

  float voltage = refVoltage / numADC_Steps * (float) measure; /* Conversion in volt */

  float resistance = Rdiv * (voltage / (refVoltage - voltage)); /* Conversion in ohm */

  record.temperatureThermistor = translation (resistance); /* Conversion in °C */

  return (true);
}

float TemperatureClient::translation(float resistance) {
  float temperature = 0.;
  //temperature = -22.098 * log(resistance) + 228.934;
  temperature = -20.4803558 * log(resistance) + 215.5535874;
  return (temperature);
}
