/*
   RF_RealisticTest_StationWithFSM.ino

   A sketch which simulates the ground station side of the RF transmission:
     - By default listen and check the received records are correct (i.e. same as EXPECTED_RECORD, except
       for timestamp, and timestamps increasing by 1).
     - Every 20 seconds, send a CMD_STRING to interrupt the emission by the can (entering command mode) until
       the acknowledge RSP_STRING is received. When received send a couple of strings and check they
       are properly echoed by the can.

     - The board waits for an actual USB Serial link, unless the SerialActivationPin is pulled down.

   WARNING: this sketch only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h.

*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "CansatRecordExample.h"
#include "elapsedMillis.h"
#include "CansatXBeeClient.h"


#ifndef RF_ACTIVATE_API_MODE
#  error "This program only works if RF_ACTIVATE_API_MODE is defined in CansatConfig.h"
#endif

// Configuration constants
#define CMD_STRING "5,12345,67890"
#define RSP_STRING "** Command received **"
#define STRING_TO_BE_ECHOED "--A string to be echoed---"
constexpr byte RF_RxPinOnUno = 9;
constexpr byte RF_TxPinOnUno = 11;
constexpr byte SerialActivationPin=6;  // If this pin is pulled-down the board will wait for Serial port initialisation,
                                       // If it is open or pulled-up, it will not
//#define SIMULATE_ON_USB_SERIAL // Define to have RF reception and transmission performed on Serial (for debugging)
constexpr unsigned long cmdPeriod = 15000; // msec
constexpr unsigned long responseDelay = 2000; // msec
constexpr unsigned long RF_BaudRate = 115200; // must be consistent with the RF module config. 9600 is not enough!
constexpr bool showBufferOverlow = true; // Set to true to get messages on Serial when the reception buffer overflows.
constexpr bool showBufferWheneverProcessed = false ; // Set to true to display the buffer content each time it is processed.
constexpr bool showIgnoredMsg = true; // Set to true to display ignored (valid) incoming messages.
constexpr bool showStateChanges = true; // Set to true to display every state change.
constexpr bool printAllCharactersReceived = false; // Set to true to have every single char received from the RF immediately printed on Serial
constexpr bool showInvalidMsg = true; // Set to true to display all invalid messages received.
//------------------------------------------------------------------------------------------

//Define a RF Serial port.
#ifdef SIMULATE_ON_USB_SERIAL
HardwareSerial &RF = Serial;
#else
#  ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
#   else
#   include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
#   endif
#endif

// -------------------------- Types -------------------------------
// The messages we recognize
typedef enum MsgType {
  None,
  Record,
  CmdAck,
  EchoedString
} MsgType_t;

// The states
typedef enum State {
  ReceivingRecords,
  WaitingForResponse,
  WaitingForEcho
} State_t;

// -------------------------- Globals -------------------------------
State_t currentState;
elapsedMillis elapsed, heartbeatElapsed;
CansatRecordExample incomingRecord;
CansatXBeeClient xbc(CanXBeeAddressSH, CanXBeeAddressSL); // Defined in CansatConfig.h

// -------------------------- End Globals ---------------------------
/* Check all data members, check timestamp is previous timestamp+1
   return true if ok */
bool checkRecord(CansatRecordExample& rec) {
  bool dataOK= rec.checkValues(true);
  static unsigned long currentTS = 0; // the timestamp expected in next record.

  bool tsOK = false;
  if (currentTS == 0) {
    currentTS = rec.timestamp + 1;
    tsOK = true;
  } else {
    if (rec.timestamp == currentTS) {
    	tsOK = true;
    } else {
      Serial << "Error in timestamp: expected " << currentTS << ", got " << rec.timestamp << ENDL;
    }
    currentTS = rec.timestamp + 1;
  }
  return (tsOK && dataOK);
}


void documentOnSerial() {
  // Document on Serial
  Serial << F("=== Simulating the RECEIVING STATION ===") << ENDL;
#ifdef SIMULATE_ON_USB_SERIAL
  Serial << F(" === Using USB Serial instead of RF Serial for test! ===") << ENDL;
#else
#endif
#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "Feather board detected: using RX-TX pins for RF communication" << ENDL;
#else
  Serial << "Assuming AVR board: using softwareSerial (rx=" << RF_RxPinOnUno
         << ", tx=" << RF_TxPinOnUno << ") for RF communication" << ENDL;
#endif
  Serial << ENDL;
  Serial << F("RF baud rate   : ") << RF_BaudRate << F(" baud") << ENDL;
  Serial << F("Expected record: '") << ENDL;
  incomingRecord.print(Serial);
  Serial << F("Sending command every ") << cmdPeriod / 1000.0 << F(" sec") << ENDL;
  Serial << F("A '.' denotes are valid record received, an 'I' denotes an invalid message") << ENDL;
}


void switchTo(State_t newState) {
  currentState = newState;
  if (showStateChanges) {
    Serial << F("Changing state to ");
    switch (newState) {
      case ReceivingRecords:
        Serial << F("ReceivingRecords");
        break;
      case WaitingForResponse:
        Serial << F("WaitingForResponse");
        break;
      case WaitingForEcho:
        Serial << F("WaitingForEcho");
        break;
      default:
        Serial << "** Unknown ** (" << newState << F(")");
    } // switch
    Serial << ENDL;
  } // if show
  elapsed = 0;
}


void ignoreMessage(MsgType_t msgType)
{
  if (showIgnoredMsg) {
    switch (msgType) {
      case None:
        break;
      case Record:
        Serial << F("Ignored incoming record") << ENDL;
        break;
      case CmdAck:
        Serial << F("Ignored cmd response") << ENDL;
        break;
      case EchoedString:
        Serial << F("Ignored echoed string") << ENDL;
        break;
      default:
        Serial << F("*** Error: ignored unexpected msgType: ") << msgType << ENDL;
    } // switch
  } // if show
} // ignoreMsg

void sendStringToEcho() {
	xbc.openStringMessage(CansatFrameType::CmdRequest);
	xbc << STRING_TO_BE_ECHOED;
	xbc.closeStringMessage();
	Serial << "String sent for echo" << ENDL;
}

void sendCommand() {
	xbc.openStringMessage(CansatFrameType::CmdRequest);
	xbc << CMD_STRING;
	xbc.closeStringMessage();
	Serial << "Command sent" << ENDL;
}

void doProcess(MsgType_t msgReceived)
{
  static int counter = 0;
  switch (currentState) {
    case ReceivingRecords:
      if (msgReceived == Record) {
        // Do not react. 
      } else {
        ignoreMessage(msgReceived);
      }
      if (elapsed >= cmdPeriod) {
		    sendCommand();
        counter = 0;
        switchTo(WaitingForResponse);
      }
      break;
    case WaitingForResponse:
      switch (msgReceived) {
        case Record:
          // Do not react.
          break;
        case CmdAck:
          Serial << F("Response received.") << ENDL;
          sendStringToEcho();
          counter = 0;
          switchTo(WaitingForEcho);
          break;
        default:
          ignoreMessage(msgReceived);
      }
      // We could have changed state already
      if ((currentState == WaitingForResponse) && (elapsed >= responseDelay)) {
        if (counter < 3) {
          sendCommand();
          counter++;
          elapsed = 0;
        } else {
          // retry exhausted.
          switchTo(ReceivingRecords);
        }
      }
      break;
    case WaitingForEcho:
      switch (msgReceived) {
        case Record:
          // Do not react
          break;
        case EchoedString:
          Serial << F("Echo received") << ENDL;
          if (counter < 3) {
			      sendStringToEcho();
            counter++;
            elapsed = 0;
          } else {
            // Done Echoing
            switchTo(ReceivingRecords);
          }
          break;
        default:
          ignoreMessage(msgReceived);
      }
      // We could have changed state already
      if ((currentState == WaitingForEcho) && (elapsed >= responseDelay)) {
        if (counter < 3) {
          sendStringToEcho();
          counter++;
          elapsed = 0;
        } else {
          // retry exhausted.
          switchTo(ReceivingRecords);
        }
      }
      break;
    default:
      Serial << F("*** Error: unexpected state: ") << currentState << ENDL;
  }  // switch(state)
} // doProcess()

void setup() {
  DINIT_IF_PIN(115200,SerialActivationPin);
  documentOnSerial();

  RF.begin(RF_BaudRate);
  pinMode(LED_BUILTIN, OUTPUT);
  xbc.begin(RF);
  currentState = ReceivingRecords;
  elapsed = 0;
  heartbeatElapsed = 0;

  Serial << F("Setup complete") << ENDL;
}

void loop() {
	bool gotRecord;
	char incomingString[xbc.MaxStringSize + 1];
	CansatFrameType stringType;
 uint8_t seqNbr;
	while (xbc.receive(incomingRecord, incomingString, stringType, seqNbr, gotRecord)) {
		if (gotRecord) {
			if (checkRecord(incomingRecord)) {
			  Serial << ".";
			} else {
        Serial << "I";
			}
			doProcess(Record);
		} else {
			switch (stringType) {
			case CansatFrameType::CmdResponse:
				if (strcmp(incomingString, RSP_STRING) == 0) {
					doProcess(CmdAck);
				} else if (strcmp(incomingString, STRING_TO_BE_ECHOED) == 0) {
					doProcess(EchoedString);
				} else {
					Serial << "Received '" << incomingString
							<< "' CmdResponse (ignored)" << ENDL;
				}
				break;
			case CansatFrameType::CmdRequest:
				Serial << "Received unexpected '" << incomingString
						<< "' CmdRequest (ignored)" << ENDL;
				break;
			default:
				Serial << "***Error: unexpected string type: "
						<< (int) stringType << ENDL;
			}
		}
	} // While receive();
	if (heartbeatElapsed > 500) {
		digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
		heartbeatElapsed = 0;
	}
}
