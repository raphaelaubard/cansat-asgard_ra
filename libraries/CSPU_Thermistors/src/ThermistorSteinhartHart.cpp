/*
   ThermistorSteinhartHart.cpp
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "ThermistorSteinhartHart.h"

#define DEBUG_CSPU
#include "DebugCSPU.h"
#define DBG_READ  0

ThermistorSteinhartHart::ThermistorSteinhartHart(
			const float theVcc, const uint8_t theAnalogPinNbr, const float theRefResistor,
			const float theA, const float theB, const float theC, const float theD,
			const float theSerialResistor) :
			Thermistor(theVcc, theAnalogPinNbr, theSerialResistor ){
  r25 = theRefResistor;
  A = theA;
  B = theB;
  C = theC;
  D = theD;
}

float ThermistorSteinhartHart::readTemperature()const {
  float R = readResistance();
  DPRINTS(DBG_READ, "R25=");
  DPRINT(DBG_READ, r25);
  DPRINTS(DBG_READ, " R=");
  DPRINTLN(DBG_READ, R);
  float logRRref = log(R / r25);
  float temp = 1 / ((((D * logRRref + C) * logRRref + B ) * logRRref) + A);
  return temp - 273.15;
}
