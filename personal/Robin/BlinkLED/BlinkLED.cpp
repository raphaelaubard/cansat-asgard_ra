#include"BlinkLED.h"

bool BlinkLED::begin(const uint8_t theLED_PinNbr, const uint16_t thePeriodInMsec) {
  pinMode (theLED_PinNbr, OUTPUT);
  LED_PinNbr = theLED_PinNbr;
  periodInMsec = thePeriodInMsec;
  lastChangeTimestamp = 0;
  return true;
}
void BlinkLED::run() {
  unsigned long now = millis();
  if ((now - lastChangeTimestamp) > periodInMsec ) {
    if (digitalRead(LED_PinNbr) == HIGH) {
      digitalWrite(LED_PinNbr, LOW);
    }
    else {
      digitalWrite(LED_PinNbr, HIGH);

    }
    lastChangeTimestamp = now;
  }

}
