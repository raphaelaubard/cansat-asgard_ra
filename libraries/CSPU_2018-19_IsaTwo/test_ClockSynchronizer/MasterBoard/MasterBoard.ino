/*
 *  test_ClockSynchronizer/MasterBoard.ino
 *  
 *  ClockSynchronizer is tested with 2 boards, connected through their I2C bus. 
 *  MasterBoard is the master program: it
 *    - sets master clock during init
 *    - prints clock regularly
 *    - resyncs every ResyncDelay msec.
 *  SlaveBoard is the slave program: it
 *    - initializes the ClockSynchronizer_Slave
 *    - Waits up to 10 sec for the master to perform synchronisation
 *    - Prints the clock regularly
 *    if CHECK_RESYNC is defined, it resets the synchronization every 10 secs in
 *    order to check the resynchronisation by the master actually happens.
 *    
 *  It should result in properly synchronized boards...
 *  
 *  
 *  Wiring: 
 *    Gnd to ground
 *    SDA to SDA  (A4 on Uno)
 *    SCL to SCL  (A5 on Uno)
 */

 #define DEBUG_CSPU
 #include "DebugCSPU.h"

 #include "ClockSynchronizer.h"
 #include "Wire.h"

 constexpr byte SlaveAddress=0xBB;
 constexpr byte InexistentSlaveAddress=0xAB;
 constexpr unsigned long ResyncDelay=5000; // msec.
 
 unsigned long ts;
 
 void setup() {
  DINIT(115200);
  Wire.begin();
  Serial << "Trying to synchronize with inexistant slave, no retry..." ;
  ts=millis();
  bool result=ClockSynchronizer_Master::setMasterClock(InexistentSlaveAddress,0);
  unsigned long duration = millis()-ts;
  if (!result) {
    Serial << "OK. (duration=" << duration << " msec)" << ENDL;
  } else {
    Serial << " *** Error: this should have failed *** " << ENDL;
  }
  
  Serial << "Synchronizing real slave, retrying for 10 seconds...";
  ts=millis();
  result=ClockSynchronizer_Master::setMasterClock(SlaveAddress, 10);
  duration = millis()-ts;
  if (!result) {
    Serial << "*** Failed. (duration=" << ((float) duration)/1000.0f << " sec)" << ENDL;
  } else {
    Serial << "OK." << ENDL;
  }Serial << "Master after setMasterClock(): duration=" << duration << "msec, OK=" << result << ENDL;
  ts=millis();
}

void loop() {
  Serial << "Master clock: " << millis() << ENDL;
  if ((millis() - ts) > ResyncDelay) {
    Serial << "Master: resynchronizing..." << ENDL;
    bool result=ClockSynchronizer_Master::setMasterClock(SlaveAddress);
    Serial << "Resync result OK =" << result << ENDL;
    ts=millis();
  }
  delay(500);
}
