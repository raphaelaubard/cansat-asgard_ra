/*
   This a template for the main sketch running on-board the can and makes use of all
   the reusable code from previous projets.
   
   It should provide a boiler-plan for a fully operational CanSat supporting the
   primary mission. 
   
   It should obviously be tailored for the specific sensors used by any particular
   project and completed for the secondary mission (see "$$$" tags).
*/
// Disable warnings caused during the Arduino includes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "CansatConfig.h"		// Keep this include as first one: it includes DebugCSPU.h, Timer.h etc with the
								// appropriate configuration.
// $$$ possibly use your project configuration file here
#include "CansatAcquisitionProcess.h" // $$$ use your projects subclass instead
#include "RT_CanCommander.h"	// $$$ possibly use your project's subclass instead, if any.
#include "CansatInterface.h"
#ifdef RF_ACTIVATE_API_MODE
#include "CansatXBeeClient.h"
#else
#include "LineStream.h"
#endif

#define DBG_RECEPTION 0
#define DBG_DIAGNOSTIC 1

// Utility macroes to process the thermistor information in the setup.
#define QUOTE(x) #x
#define STR(x) QUOTE(x)

CansatAcquisitionProcess process; // $$$ Use your project's subclass here to populate your record with secondary mission data.
RT_CanCommander commander(RT_CommanderTimeoutInMsec); // $$$ Possibly use you project's subclass if it supports project-specific commands.

#ifdef RF_ACTIVATE_API_MODE
CansatXBeeClient* rf = NULL;
char msg[CansatXBeeClient::MaxStringSize+1];
CansatFrameType stringType;
uint8_t sequenceNbr;
#else
LineStream lineRF_Stream;
Stream* rf = NULL;
#endif

#ifdef INIT_SERIAL
void printWelcomeBoard() { // $$$ Print the name of your project...
  Serial << "============================================" << ENDL;
  Serial << "=           Cansat Project CSPU            =" << ENDL;
  Serial << "============================================" << ENDL << ENDL;
  Serial << " #### #### #### #### #### #### #### #### #### #### " << ENDL;
  Serial << "   #   #     #    #    #   #     #   #     #   #"    << ENDL;
  Serial << "    # #       #  #      # #       # #       # #"     << ENDL;
  Serial << "     #         #         #         #         #"      << ENDL;
  Serial << "    # #       # #       # #       # #       # #   "  << ENDL;
  Serial << "   #   #     #    #    #   #     #   #     #   #"    << ENDL;
  Serial << " #### #### #### #### #### #### #### #### #### #### " << ENDL << ENDL;
#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "*** Warning: Not running on Feather M0 Express board ??? ***" << ENDL << ENDL;
#endif
  Serial << "Hardware pins : " << ENDL;
  Serial << "  Serial debug (active LOW): A" << DebugCtrlPinNumber -A0 << ENDL;
  Serial << "  Thermistors:           1 : " << STR(THERMISTOR1_CLASS) <<
		  ", pin=A" << Thermistor1_AnalogInPinNbr - A0 << ", Serial="
		  	  	   << Thermistor1_Resistor << " Ohms" << ENDL;
#ifdef INCLUDE_THERMISTOR2
  Serial << "                         2 : " << STR(THERMISTOR2_CLASS)
		 << ", pin=A" << Thermistor2_AnalogInPinNbr - A0 << ", Serial="
		 << Thermistor2_Resistor << " Ohms" << ENDL;
#endif
#ifdef INCLUDE_THERMISTOR3
  Serial << "                         3 : " << STR(THERMISTOR3_CLASS)
		 << ", pin=A" << Thermistor3_AnalogInPinNbr - A0 << ", Serial="
		 << Thermistor3_Resistor << " Ohms" << ENDL;
#endif
  Serial << "  SD Card chip-select      : " <<  SD_CardChipSelect <<  ENDL;

  Serial << "Reference tension          : " << ThermistorTension << "V" << ENDL;
  Serial << "RT-Commander request code  : " << (byte) CansatFrameType::CmdRequest << ENDL;
  Serial << "Acquisition period         : " << CansatAcquisitionPeriod << " msec " << ENDL;
  Serial << ENDL;
} // PrintWelcomeBoard
#endif

void setup() {
#ifdef INIT_SERIAL
  DINIT_IF_PIN(USB_SerialBaudRate, DebugCtrlPinNumber);
  printWelcomeBoard();
#endif
  process.init();
  DPRINTSLN(DBG_SETUP, "AcquisitionProcess initialized");

  CansatHW_Scanner* hw = process.getHardwareScanner();
#ifdef RF_ACTIVATE_API_MODE
  rf = hw->getRF_XBee();
#else
  rf = hw->getRF_SerialObject();
#endif

  // if the RF interface is available, we use the RT-Commander,
  // otherwise, just run the AcquisitionProcess
  if (rf) {
#ifndef RF_ACTIVATE_API_MODE
    lineRF_Stream.begin(*rf, MaxUplinkMsgLength);
#endif
    commander.begin(*rf, process.getSdFat() , &process);
    DPRINTSLN(DBG_SETUP, "RT - Commander initialized");
  } else {
    DPRINTSLN(DBG_SETUP, "RT - Commander NOT IN USE");
  }
  DPRINTSLN(DBG_SETUP, "Setup complete");
  DPRINTSLN(DBG_SETUP, "------------------");
}

void loop() {
	DPRINTSLN(DBG_LOOP, "*** Entering processing loop");
	if (rf) {
#ifdef RF_ACTIVATE_API_MODE
		if (rf->receive(msg, stringType, sequenceNbr)) {
			DPRINTSLN(DBG_RECEPTION, "Received string");

			// Process incoming message.
			switch (stringType) {
			case CansatFrameType::CmdResponse:
				DPRINTSLN(DBG_DIAGNOSTIC,
						"*** Received a Cmd Response ?? Ignored.")
				break;
			case CansatFrameType::StatusMsg:
				DPRINTSLN(DBG_DIAGNOSTIC,
						"*** Received a Status msg ?? Ignored.")
				break;
			case CansatFrameType::CmdRequest:
				// This is the only msg type the can should expect.
				break;
			default:
				DPRINTS(DBG_DIAGNOSTIC,
						"*** Error: unexpected StringType received (ignored):")
				DPRINT(DBG_DIAGNOSTIC, (int ) stringType)
				DPRINTS(DBG_DIAGNOSTIC, ", ")
				DPRINTLN(DBG_DIAGNOSTIC, msg)
				;
			} // switch
		} // anything string received.
#else
		const char* msg = lineRF_Stream.readLine();
#endif
		if ((msg != nullptr) && (*msg != '\0')) {
			DPRINTS(DBG_LOOP_MSG, "Received '");
			DPRINT(DBG_LOOP_MSG, msg);
			DPRINTLN(DBG_LOOP_MSG, "'");
			commander.processCmdRequest(msg);
#ifdef RF_ACTIVATE_API_MODE
			*msg = '\0'; // reset msg buffer to avoid processing it again.
#endif
		}
		if (commander.currentState() == RT_CanCommander::State_t::Acquisition) {
			process.run();
		}
	} else 	{
		// rf not available.
		process.run();
	}
	DDELAY(DBG_LOOP, 500);  // Slow main loop down for debugging.
	DPRINTSLN(DBG_LOOP, "*** Exiting processing loop");
}
