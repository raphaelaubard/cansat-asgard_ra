/**
   XBeeClient.cpp
   Started on 21 April 2019
*/
// Silence warnings in standard arduino files
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-parameter"
#include "Arduino.h"
#pragma GCC diagnostic pop

#include "XBeeClient.h" // This one must be outside the ifdef to have the symbol defined or undefined.

#ifdef RF_ACTIVATE_API_MODE

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include <XBee.h>

#define DBG_SEND 0
#define DBG_RECEIVE 0
#define DBG_STREAM 0
#define DBG_DIAGNOSTIC 1

XBeeClient::XBeeClient(uint32_t SH, uint32_t SL) :
  xbee(),
  defaultReceiverAddress(SH, SL),
  msgBuffer(),
  sstr(msgBuffer)
{
  msgBuffer.reserve(200);
}

void XBeeClient::begin(Stream &stream) {
  xbee.begin(stream);
}

bool XBeeClient::send(	uint8_t* data,
                        uint8_t dataSize,
                        int timeOutCheckResponse,
                        uint32_t SH, uint32_t SL) {
  if (dataSize > MaxPayloadSize) {
    DPRINTS(DBG_DIAGNOSTIC, "XBeeClient::send: payload too large: ");
    DPRINT(DBG_DIAGNOSTIC, dataSize);
    DPRINTSLN(DBG_DIAGNOSTIC, " bytes");
    return false;
  }
  XBeeAddress64 receiverAddress(SH, SL);
  if ((SH == 0L) && (SL == 0L)) {
    receiverAddress = defaultReceiverAddress;
  }

  if (DisplayOutgoingFramesOnSerial) {
	  Serial << "Sending..." << ENDL;
	  displayFrame(data,dataSize);
  }

  // Should we give values back ? Like the SD_Logger for instance to know exactly the type of ***ERROR*** it is
  bool result = false;
  ZBTxRequest zbTx = ZBTxRequest(receiverAddress, data, dataSize); //Creating an object to send the data
  ZBTxStatusResponse txStatus = ZBTxStatusResponse();
  xbee.send(zbTx);
  if (timeOutCheckResponse == 0) {
    // If timeout is null we do not check for reception.
    return true;
  }

  if (xbee.readPacket(timeOutCheckResponse)) { //Wait up to 500 ms a response from the other Xbee that a packet was received - Change value to variable given by user
    if (xbee.getResponse().getApiId() == ZB_TX_STATUS_RESPONSE) {
      DPRINTS(DBG_SEND, "We got the ZB_TX_STATUS_RESPONSE...");
      xbee.getResponse().getZBTxStatusResponse(txStatus);
      // get the delivery status, the fifth byte

      uint8_t status = txStatus.getDeliveryStatus();
      DPRINTS(DBG_SEND, "Delivery status = ");
      DPRINTLN(DBG_SEND, status);

      if (txStatus.getDeliveryStatus() == SUCCESS) {
        DPRINTSLN(DBG_SEND, "SUCCESS");
        result = true;
      } else {
        // ***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** The other XBee didn't receive our packet");
      }
    }
  } else if (xbee.getResponse().isError()) {
    // ***ERROR*** we had no response
    DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** reading packet. ErrCode=");
    DPRINTLN(DBG_SEND, xbee.getResponse().getErrorCode());
    result = DBG_DIAGNOSTIC;
  } else {
    // ***ERROR***
    DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** - No timely TX Status Response");
  }
  return result;
}

bool XBeeClient::receive(uint8_t* &payload, uint8_t& payloadSize) {
  bool result = false;
  static ZBRxResponse rx; 	// Make it static to preserve the object and the buffer which we return as payload.
  	  	  	  	  	  	  	// Could be unnecessary if the internal buffer is a global, but who knows...
  ModemStatusResponse msr;
  xbee.readPacket(); //Checks the communication at every loop
  if (xbee.getResponse().isAvailable()) {
    DPRINTS(DBG_RECEIVE, "We got something: ");
    uint8_t apiId = xbee.getResponse().getApiId();
    if (apiId == ZB_RX_RESPONSE) {
      DPRINTSLN(DBG_RECEIVE, "a zb rx packet!");
      //Now fill our zb rx class
      xbee.getResponse().getZBRxResponse(rx);
      uint8_t resp = rx.getOption();
      if (resp == ZB_PACKET_ACKNOWLEDGED) {
        // the sender got an ACK
        DPRINTSLN(DBG_RECEIVE, "Sender got en ACK - good");

        payload = rx.getData();
        payloadSize = rx.getDataLength();
        if (DisplayIncomingFramesOnSerial) {
        	  Serial << "Received payload:" << ENDL;
        	  displayFrame(payload,payloadSize);
          }
        result = true;
      } else {
        //***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "***ERROR*** Received packet but sender didn't get an ACK. API ID=");
        DPRINTLN(DBG_DIAGNOSTIC, apiId);
        DPRINTS(DBG_DIAGNOSTIC, ", ZBRxResponse: ");
        DPRINTLN(DBG_DIAGNOSTIC, resp);
      }
    } else if (apiId == MODEM_STATUS_RESPONSE) {
      xbee.getResponse().getModemStatusResponse(msr);
      // the local XBee sends this response on certain events, like association/dissociation
      DPRINTS(DBG_RECEIVE, "A certain event happened association/dissociation: ");
      auto msrStatus = msr.getStatus() ;
      if (msrStatus == ASSOCIATED) {
        DPRINTSLN(DBG_RECEIVE, "It's an association - Yeay this is great!");
        result = true;
      } else if (msrStatus == DISASSOCIATED) {
        //***ERROR***
        DPRINTSLN(DBG_DIAGNOSTIC, "*** Error MSR status: dissociation");
      } else {
        //***ERROR***
        DPRINTS(DBG_DIAGNOSTIC, "*** Error MSR status unknown: ");
        DPRINTLN(DBG_DIAGNOSTIC, msrStatus);
      }
    }
    else if (apiId == ZB_TX_STATUS_RESPONSE) {
      // This we receive after a message is sent for which we did not wait for
      // the TX_STATUS_RESPONSE because the user did not require an ack.
      // At this stage the message is just ignored.
      DPRINTSLN(DBG_RECEIVE, "(ignored) ZB_TX_STATUS_RESPONSE ");
    }
    else {
      DPRINTS(DBG_RECEIVE, "(ignored) Unexpected API ID=0x");
      DPRINTLN(DBG_RECEIVE, apiId, HEX);
    }
  } else if (xbee.getResponse().isError()) {
    //***ERROR***
    DPRINTS(DBG_DIAGNOSTIC, "*** ERROR in reading packet. Err code=");
    DPRINTLN(DBG_DIAGNOSTIC, xbee.getResponse().getErrorCode());
    switch (xbee.getResponse().getErrorCode()) {
      case CHECKSUM_FAILURE:
        DPRINTLN(DBG_DIAGNOSTIC, "Checksum failure");
        break;
      case PACKET_EXCEEDS_BYTE_ARRAY_LENGTH:
        DPRINTS(DBG_DIAGNOSTIC, "Packet exceeds byte-array length. Should not exceed (bytes)  ");
        DPRINTLN(DBG_DIAGNOSTIC, MAX_FRAME_DATA_SIZE - 10);
        break;
      case UNEXPECTED_START_BYTE:
        DPRINTLN(DBG_DIAGNOSTIC, "Unexpected start byte");
        break;
      default:
        DPRINTLN(DBG_DIAGNOSTIC, "Unknown error code.");
    }
  }
  return result;
}

void XBeeClient::openStringMessage(uint8_t type, uint8_t seqNbr) {
  msgBuffer = "  ";		// Reserve 2 bytes.
  msgBuffer[0] = type; // Store value of byte, not string representation.
  msgBuffer[1] = seqNbr; // Optional sequence number transported in this byte
  	  	  	  	  	  	 // which is needed to keep even alignment of the string.
  DPRINTS(DBG_STREAM,  "XBeeClient::openStringMessage. buffer='");
  DPRINT(DBG_STREAM,  (msgBuffer.c_str() + 2));
  DPRINTS(DBG_STREAM,  "', type=");
  DPRINTLN(DBG_STREAM, (uint8_t) msgBuffer[0]);
}

bool XBeeClient::closeStringMessage(int timeOutCheckResponse) {
  DPRINTS(DBG_STREAM,  "XBeeClient::closeStringMessage. buffer='");
  DPRINT(DBG_STREAM,  (msgBuffer.c_str() + 2));
  DPRINTS(DBG_STREAM,  "', type=");
  DPRINTLN(DBG_STREAM, (uint8_t) msgBuffer[0]);

  auto sLength = msgBuffer.length()-2; // First 2 bytes are not the string
  if (sLength == 0) {
    DPRINTS(DBG_DIAGNOSTIC, "Error: closing empty message. Type=");
    DPRINTLN(DBG_DIAGNOSTIC, (uint8_t) msgBuffer[0]);
    return false;
  }
  if (sLength > MaxStringSize) {
    DPRINTS(DBG_DIAGNOSTIC, "Error: string message too long:");
    DPRINT(DBG_DIAGNOSTIC, sLength);
    DPRINTS(DBG_DIAGNOSTIC, ", Max=");
    DPRINT(DBG_DIAGNOSTIC, MaxStringSize);
    DPRINTS(DBG_DIAGNOSTIC, ", ");
    DPRINTLN(DBG_DIAGNOSTIC, (msgBuffer.c_str() + 2));
    return false;
  }

  uint8_t* payload = (uint8_t *) msgBuffer.c_str();
  uint8_t payloadSize = (uint8_t) msgBuffer.length() + 1;
  if (DisplayOutgoingFramesOnSerial) {
 	  displayString(payload, payloadSize);
  }

  // Also send the terminating '\0'.
  return send(payload, payloadSize, timeOutCheckResponse);
}

bool XBeeClient::getString(	char* str, uint8_t &stringType,
							uint8_t& stringSeqNbr,
							const uint8_t* data, uint8_t dataSize) const {
	bool result=false;

	if (dataSize <=2) {
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: received empty string");
		*str ='\0';
		return result;
	}
	const char* s = (const char*) (data + 2);
    stringType= data[0];
    stringSeqNbr=data[1];

    if (dataSize != (uint8_t) strlen(s) + 3) {
		// 2 bytes + string + '\0'
		DPRINTS(DBG_DIAGNOSTIC, "*** Error: inconsistent size. size=");
		DPRINT(DBG_DIAGNOSTIC, dataSize);
		DPRINTS(DBG_DIAGNOSTIC, ", String size, from third byte=");
		DPRINTLN(DBG_DIAGNOSTIC, strlen(s));
	} else {
		memcpy(str, data+2, strlen(s)+1);
		result=true;
	}

	return result;
}
#endif
