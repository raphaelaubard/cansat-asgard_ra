/*
 * EEPROM_Bank.cpp
 * 

 * This is a subclass of EEPROM_BankWriter completed with methods required for reading. */

#include "EEPROM_Bank.h"
//#define DEBUG_CSPU
#define USE_ASSERTIONS
#include "DebugCSPU.h"
#define DBG_READ_WRITE 0
#define DBG_READ_DATA 0
#define DBG_READER_AT_END 0

// ----------------- Constructor & overloaded methods ---------------

EEPROM_Bank::EEPROM_Bank(const unsigned int theMaintenancePeriodInSec)
	: EEPROM_BankWriter(theMaintenancePeriodInSec)
{
  readerCurrentAddress = 0;
  readerCurrentChip = 0;
}

bool EEPROM_Bank::init(const EEPROM_Key theKey, const HardwareScanner &hw, const byte recordSize)
{
	bool result=EEPROM_BankWriter::init(theKey, hw, recordSize);
	if (result) {
		resetReader();
	}
	return result;
}
// ------------------ Reader methods ---------------------------

void EEPROM_Bank::resetReader() {
  readerCurrentChip = getFirstEEPROM_Chip();
  readerCurrentAddress = sizeof(EEPROM_Header);
}

byte EEPROM_Bank::readData(byte* data, const byte dataSize) {
  if (readerAtEnd()) return 0;
  byte sizeToRead = dataSize;
  unsigned long sizeLeft = leftToRead(); 
  byte sizeRead;
  const HardwareScanner* hardware=getHardware();
  if (sizeLeft < dataSize) sizeToRead = sizeLeft;
  if (sizeLeft <= 0) return 0;

  if (sizeToRead <= (getHeader().chipLastAddress - readerCurrentAddress +1)) {
    DPRINTS(DBG_READ_DATA, "Reading from single chip #");
    DPRINT(DBG_READ_DATA, readerCurrentChip);
    DPRINTS(DBG_READ_DATA, " at 0x");
    DPRINTLN(DBG_READ_DATA, readerCurrentAddress, HEX);
    sizeRead = readFromEEPROM(hardware->getExternalEEPROM_I2C_Address(readerCurrentChip), readerCurrentAddress, data, sizeToRead);
    readerCurrentAddress += sizeRead;
    return sizeRead;
  }
  else {
    DPRINTSLN(DBG_READ_DATA, "Reading from 2 chips");
    byte size1 = getHeader().chipLastAddress - readerCurrentAddress + 1;
    byte sizeRead2;
    sizeRead = readFromEEPROM(hardware->getExternalEEPROM_I2C_Address(readerCurrentChip), readerCurrentAddress, data, size1);
    DASSERT(sizeRead == size1);
    readerCurrentChip = nextChip(readerCurrentChip);
    readerCurrentAddress=0;
    DASSERT(readerCurrentChip != 0);
    sizeRead2 = readFromEEPROM(hardware->getExternalEEPROM_I2C_Address(readerCurrentChip), 0, &data[sizeRead], sizeToRead - size1);
    DASSERT(sizeRead2 = sizeToRead - size1);
    readerCurrentAddress = sizeRead2;
#if DBG_READ_WRITE==1
    hexDumpBuffer(data, sizeToRead);
#endif
    return sizeToRead;
  }
}

bool EEPROM_Bank::readOneRecord(byte* data, const byte dataSize)
{
  DASSERT(dataSize==getHeader().recordSize);
  bool result= (dataSize == readData(data, dataSize));
  return result;
}

bool EEPROM_Bank::readerAtEnd() {
  const EEPROM_Header header=getHeader();
  const byte firstEEPROM_Chip=getFirstEEPROM_Chip();
  DASSERT(readerCurrentChip <= (firstEEPROM_Chip+header.firstFreeChip));
  bool inLastChip = (readerCurrentChip == (firstEEPROM_Chip+header.firstFreeChip));
  bool atEnd= inLastChip && (readerCurrentAddress >= header.firstFreeByte);
  DPRINTS(DBG_READER_AT_END, "inLastChip/atEnd:");
  DPRINT(DBG_READER_AT_END, inLastChip);
  DPRINTS(DBG_READER_AT_END, "/");
  DPRINTLN(DBG_READER_AT_END, atEnd);
  
  return atEnd;
}

unsigned long EEPROM_Bank::leftToRead() {
  unsigned long dataSize = 0;
  byte numChips=getFirstEEPROM_Chip() - readerCurrentChip;
  
  // If more than 1 chip to consider, disregard last chip here.
  if (numChips>=1) numChips--;
  dataSize=numChips*(getHeader().chipLastAddress+1);

  // Add data in the last chip.
  dataSize+=getHeader().firstFreeByte;
  
  // Substract data already read. NB: do this as last step, otherwise
  // the value could become < 0 which is not good for an unsigned int...
  // It includes the header if readerCurrentChip == firstEEPROM_Chip.
  // If not, we need the header is not relevant. 
  dataSize-=readerCurrentAddress;

  return dataSize;
}

unsigned long EEPROM_Bank::recordsLeftToRead() {
  unsigned long left=leftToRead();
  
  DASSERT((left % getHeader().recordSize) == 0);
  return (left / (getHeader().recordSize));
}

