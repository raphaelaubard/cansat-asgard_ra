/*
 * This is a configuration file, global for the sketch. It is included by *every* source 
 * file in the sketch and is the preferred place to 
 * gather all configuration flags and constants that are not private to a single source file,
 * e.g. the various trace/debug settings, and the include of DebugCSPU.h. 
 */
 
#pragma once          // Make sure this include file is included only once 
#include "Arduino.h"  // All standard Arduino objects, definitions, functions etc. 

// ----------- DebugCSPU controls --------------------
// (1) This is the main switch for the debugging code. Comment out to strip out debugging code. 
//#define DEBUG_CSPU
// (2) This is the switch to limit the debugging code to assertions only. 
//#define USE_ASSERTIONS
#define ACTIVATION_PIN 3

#include "DebugCSPU.h" // Always include, even if DEBUG_CSPU is not defined!
                          // Always include AFTER definition of DEBUG_CSPU
// (2) Select which part of the code to debug. 0=off, 1=on. Add new symbols  as needed. 
#define DBG_LOOP   0
#define DBG_SETUP  1
#define DBG_SRC2   0
// ------------------------------------------------------

// Test program controls: Use to check that code size reduces accordingly.
// Without any symbol defined, the sketch should be empty.
#define TEST_CONDITIONAL_INIT  // If defined, the Serial port is initialized if pin
                               // ACTIVATION_PIN is connected to GND only. 
#define TEST_FAILED_ASSERTION  // If defined, the program aborts at the end of teh loop() `
                               // function because of a failed assertion.
//#define INIT_SERIAL       // If undefined, the serial interface is not initialized.
//#define CALL_LIBRARIES    // If undefined, library functions are not called.
//#define DO_SOMETHING_LOCAL     // If undefined, functions called locally within the sketch are not called.
