/* Test configuration of TC timers on SAMD:
    source: https://gist.github.com/jdneo/43be30d85080b175cb5aed3500d3f989

    This code works perfectly for periods from 1 ms to 1000 ms on TC3.
*/
#define LED_PIN 13

#define CPU_HZ 48000000
#define TIMER_PRESCALER_DIV 1024

void startTimer(int frequencyHz);
void setTimerFrequency(int frequencyHz);
void TC3_Handler();

void setTimerFrequency(int frequencyHz) {
  int compareValue = (CPU_HZ / (TIMER_PRESCALER_DIV * frequencyHz)) - 1;
  TcCount16* TC = (TcCount16*) TC3;
  // Make sure the count is in a proportional position to where it was
  // to prevent any jitter or disconnect when changing the compare value.
  TC->COUNT.reg = map(TC->COUNT.reg, 0, TC->CC[0].reg, 0, compareValue);
  TC->CC[0].reg = compareValue;
  Serial.println(TC->COUNT.reg);
  Serial.println(TC->CC[0].reg);
  while (TC->STATUS.bit.SYNCBUSY == 1);
}

void startTimer(int frequencyHz) {
  REG_GCLK_CLKCTRL = (uint16_t) (GCLK_CLKCTRL_CLKEN | GCLK_CLKCTRL_GEN_GCLK0 | GCLK_CLKCTRL_ID_TCC2_TC3) ;
  Serial.println(REG_GCLK_CLKCTRL);
  while ( GCLK->STATUS.bit.SYNCBUSY == 1 ); // wait for sync

  TcCount16* TC = (TcCount16*) TC3;

  TC->CTRLA.reg &= ~TC_CTRLA_ENABLE;
  while (TC->STATUS.bit.SYNCBUSY == 1); // wait for sync

  // Use the 16-bit timer
  TC->CTRLA.reg |= TC_CTRLA_MODE_COUNT16;
  while (TC->STATUS.bit.SYNCBUSY == 1); // wait for sync

  // Use match mode so that the timer counter resets when the count matches the compare register
  TC->CTRLA.reg |= TC_CTRLA_WAVEGEN_MFRQ;
  while (TC->STATUS.bit.SYNCBUSY == 1); // wait for sync

  // Set prescaler to 1024
  TC->CTRLA.reg |= TC_CTRLA_PRESCALER_DIV1024;
  while (TC->STATUS.bit.SYNCBUSY == 1); // wait for sync

  setTimerFrequency(frequencyHz);

  // Enable the compare interrupt
  TC->INTENSET.reg = 0;
  TC->INTENSET.bit.MC0 = 1;

  NVIC_EnableIRQ(TC3_IRQn);

  TC->CTRLA.reg |= TC_CTRLA_ENABLE;
  while (TC->STATUS.bit.SYNCBUSY == 1); // wait for sync

  Serial.print("COUNT16.CTRLA.reg=");
  Serial.println(TC->CTRLA.reg);
  Serial.print("COUNT16.INTENSET.reg=");
  Serial.println(TC->INTENSET.reg);
}

//----------------
constexpr uint16_t TC3_timerPeriod = 1000;     // Timer period in milliseconds, determines how often the interrupt is triggered
constexpr uint16_t TC4_timerPeriod = 500;     // Timer period in milliseconds, determines how often the interrupt is triggered
constexpr uint16_t TC5_timerPeriod = 1000;     // Timer period in milliseconds, determines how often the interrupt is triggered
constexpr uint32_t reportingPeriod = 3000; // Report frequency each reportingPeriod msec.
constexpr byte NumChecksPerTest = 10;     // Number of checks between two successive counter resets.


uint32_t counterTC3 = 0; // count the number of interrupts to check frequency
uint32_t counterTC4 = 0; // count the number of interrupts to check frequency
uint32_t counterTC5 = 0; // count the number of interrupts to check frequency
uint32_t startTime = 0; // the clock value when the counter is reset.

void TC3_Handler() {
  TcCount16* TC = (TcCount16*) TC3;
  // If this interrupt is due to the compare register matching the timer count
  // we increase counter
  if (TC->INTFLAG.bit.MC0 == 1) {
    TC->INTFLAG.bit.MC0 = 1;
    counterTC3++;
  }
}

void TC5_Handler (void) {
  counterTC5++;


}

void TC4_Handler (void) {
  counterTC4++;

}


void setup() {
  Serial.begin(115200);
  while (!Serial) ;
  startTimer(1000 / TC3_timerPeriod);
}

void checkTimerPeriod() {
  // Evaluate the average period of the timer.
  auto now = millis();
  auto duration = now - startTime;
  float periodTC3 = ((float) duration) / counterTC3;
  float periodTC4 = ((float) duration) / counterTC4;
  float periodTC5 = ((float) duration) / counterTC5;
  Serial.println(counterTC3);
  Serial.print("After ");
  Serial.print(duration / 1000);
  Serial.print(" sec, average period: ");
  Serial.print("TC3=");
  Serial.print(periodTC3);
  Serial.print(" msec (expected ");
  Serial.print(TC3_timerPeriod);
  Serial.print("), ");
  Serial.print("TC4=");
  Serial.print(periodTC4);
  Serial.print(" msec (expected ");
  Serial.print(TC4_timerPeriod);
  Serial.print("), TC5=");
  Serial.print(periodTC5);
  Serial.print(" msec (expected ");
  Serial.print(TC5_timerPeriod);
  Serial.println(")");
}

void loop() {
  static uint16_t i = 0;
  if ((i % (NumChecksPerTest + 1)) == 0) {
    // start new test
    startTime = millis();
    counterTC3 = counterTC4 = counterTC5 = 0;
    Serial.println("------");
  }
  else checkTimerPeriod();
  i++;
  delay(reportingPeriod);
}
