/* Test for the various symbols used to discriminate between boards/architectures/processor.
 * ItsyBitsy M4 compilation */

#include "Arduino.h"

#ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#error Feather M0 express
#else
#error not "Feather M0 express"
#endif

#ifdef ARDUINO_ITSYBITSY_M4
#error ARDUINO_ITSYBITSY_M4
#else
#error "not ARDUINO_ITSYBITSY_M4"
#endif

#ifdef ARDUINO_ITSYBITSY_M0
#error ARDUINO_ITSYBITSY_M0
#else
#error "not ARDUINO_ITSYBITSY_M0"
#endif

#ifdef ARDUINO_ARCH_SAMD
#error ARDUINO_ARCH_SAMD
#else
#error "not ARDUINO_ARCH_SAMD"
#endif

#ifdef ARDUINO_AVR_UNO
#error ARDUINO_AVR_UNO
#else
#error "not ARDUINO_AVR_UNO"
#endif

#ifdef __AVR__
#error __AVR__
#else
#error "not __AVR__"
#endif
#ifdef __SAMD21G18A__
#error "__SAMD21G18A__"
#else
#error "not __SAMD21G18A__"
#endif

#ifdef __SAMD51G__
#error "__SAMD51G__"
#else
#error "not __SAMD51G__"
#endif

#ifdef __SAMD51G18A__
#error "__SAMD51G18A__"
#else
#error "not __SAMD51G18A__"
#endif

#ifdef __SAMD51__
#error __SAMD51__
#else
#error "not __SAMD51__"
#endif

#ifdef __SAMD21__
#error __SAMD21__
#else
#error "not __SAMD21_ (never set)._"
#endif

#ifdef __ATSAMD51G19A__
#error __ATSAMD51G19A__
#else
#error "not __ATSAMD51G19A__"
#endif

#ifdef __ATSAMD51J20A__
#error __ATSAMD51J20A__
#else
#error "not __ATSAMD51J20A__"
#endif
#ifdef ARDUINO_SAMD_ZERO
#error "ARDUINO_SAMD_ZERO"
#else
#error "not ARDUINO_SAMD_ZERO"
#endif


void setup() {}

void loop() {}
