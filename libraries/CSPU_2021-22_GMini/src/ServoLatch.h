#pragma once
#include <Servo.h>
#include "Arduino.h"

class ServoLatch {
  public :
    bool begin (const uint8_t PWM_Pin);
    void unlock();
    void lock();
  private :
    Servo myServo;
    const uint8_t UnLockPosition = 45;
    const uint8_t LockPosition = 0;
    const uint16_t DelayBeforeDetach = 35;
    const uint16_t DelayAfterAttach = 35;
    const uint16_t thePWM_Pin = 9;
};
