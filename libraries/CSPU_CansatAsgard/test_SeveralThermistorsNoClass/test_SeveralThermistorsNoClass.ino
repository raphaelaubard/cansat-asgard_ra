/*
 *  This program tests the following 3 thermistors: 
 *   - NTCLE100E3
 *   - NTCLG100E2
 *   - VMA320 (break-out)
 *   
 *  Wiring:
 *    VCC - NTCLE100E3 - A0 -  10kOhm - GND
 *    VCC - NTCLG100E2 - A1 - 10kOhm - GND
 *    VMA320, looking from components side, thermistor facing up:
 *      A2 to left
 *      VCC to middle 
 *      Gnd to right. 
 *      
 *   Usage: use csvFormat constant to select between human-readable output and csv output (for use in Excel sheet); 
 */
 
constexpr bool csvFormat = false;// SEt to true to generate CSV formatted output.

// Constants discribing wiring and thermistor models.
constexpr float Rref_A = 22000.0;
constexpr float NTC_A1 = 3.354016E-03;
constexpr float NTC_B1 = 2.744032E-04;
constexpr float NTC_C1 = 3.666944E-06;
constexpr float NTC_D1 = 1.375492E-07;
constexpr float Rref_B = 100000.0;
constexpr float NTC_A2 = 0.003354016;
constexpr float NTC_B2 = 0.0002569850;
constexpr float NTC_C2 = 2.620131E-06;
constexpr float NTC_D2 = 6.383091E-08;
constexpr float Rref_C = 10000.0;
constexpr float NTC_A3 = 1.009249522e-03;
constexpr float NTC_B3 = 2.378405444e-04 ;
constexpr float NTC_C3 = 2.019202697e-07 ;

#define DEBUG_CSPU
#include "DebugCSPU.h"
float Vmax;         // Ref. voltage for ADC (differs according to HW architecture)
uint16_t Nsteps;    // Number of steps of ADC (differs according to HW architecture)

// Measurement values for thermistor A
float V_A;
float R_A;
float temp_A;
float Tc_A;
int sensorValue_A;

// Measurement values for thermistor B
float V_B;
float R_B;
float temp_B;
float Tc_B;
int sensorValue_B;

// Measurement values for thermistor C
float V_C;
float R_C;
float temp_C;
float Tc_C;
int sensorValue_C;

void setup() {
  // to know wich Vmax and Nsteps we have to use;
#ifdef ARDUINO_AVR_UNO
  Vmax = 5.0;
  Nsteps = 1024;
#elif defined(ARDUINO_SAMD_FEATHER_M0_EXPRESS)
  Vmax = 3.3;
  Nsteps = 4096 ;
#else
#error "Board not recognised"
#endif

  DINIT(115200);
  if (csvFormat) {
    Serial << "time, A(NTCLE100E3)=, sensor value, voltage, resistance, temp (K), temp(°C)" ;
    Serial << ", B(NTCLG100E2)=, sensor value, voltage, resistance, temp (K), temp(°C)";
    Serial << ", C (VMA320)=, sensor value, voltage, resistance, temp (K), temp(°C)" << ENDL;
  }
}

void readDataNTCLE100E3() {
  const int Resistor2 = 10000;
  sensorValue_A = analogRead(A0);
  V_A = sensorValue_A * (Vmax / (Nsteps - 1));
  R_A = (V_A * Resistor2) / (Vmax - V_A);
  float logRRref = log(R_A / Rref_A);
  temp_A = 1 / (NTC_A1 + NTC_B1 * logRRref + NTC_C1 * logRRref * logRRref + NTC_D1 * logRRref * logRRref * logRRref);
  Tc_A = temp_A - 273.15;
}

void readDataNTCLG100E2() {
  const int Resistor2 = 10000;
  sensorValue_B = analogRead(A1);
  V_B = sensorValue_B * (Vmax / (Nsteps - 1));
  R_B = (V_B * Resistor2) / (Vmax - V_B);
  float logRRref = log(R_B / Rref_B);
  temp_B = 1 / (NTC_A2 + NTC_B2 * logRRref + NTC_C2 * logRRref * logRRref + NTC_D2 * logRRref * logRRref * logRRref);
  Tc_B = temp_B - 273.15;
}

void readDataVMA320() {
  const int Resistor2 = 10000.0;
  sensorValue_C = analogRead(A2);
  V_C = sensorValue_C * (Vmax / (Nsteps - 1));
  R_C = (V_C * Resistor2) / (Vmax - V_C);
  float logRRref = log(R_C);
  temp_C = 1 / (NTC_A3 + NTC_B3 * logRRref + NTC_C3 * (logRRref * logRRref * logRRref));
  Tc_C = temp_C - 273.15;

}
void loop() {
  // put your main code here, to run repeatedly:

  readDataNTCLE100E3();
  readDataNTCLG100E2();
  readDataVMA320();
  if (csvFormat) {
    Serial <<  millis() << "," << sensorValue_A << "," << V_A << "," << R_A << "," << temp_A  << ", " << Tc_A;
    Serial << "," << sensorValue_B << "," << V_B << "," << R_B << "," << temp_B  << ", " << Tc_B ;
    Serial << "," << sensorValue_C << "," << V_C << "," << R_C << "," << temp_C  << ", " << Tc_C << ENDL;

  } else {
    Serial <<  millis() << ", A(NTCLE100E3): value=" << sensorValue_A << ", V=" << V_A << " V, R=" << R_A << " ohms, t°=" << temp_A  << "°K, " << Tc_A  << "°C" << ENDL;
    Serial <<  millis() << ", B(NTCLG100E2): value=" << sensorValue_B << ", V=" << V_B << " V, R=" << R_B << " ohms, t°=" << temp_B  << "°K, " << Tc_B  << "°C" << ENDL;
    Serial <<  millis() << ", C(VMA320): value=" << sensorValue_C << ", V=" << V_C << " V, R=" << R_C << " ohms, t°=" << temp_C  << "°K, " << Tc_C  << "°C" <<  ENDL;
  }
  delay(1000);

}
