/** Program for RF-Transceiver station.
    Downlink: it receives the data from the RF, stores it on an SD Card and sends to
              the RT-Processing on TTL-USB Serial (data records) and command PC on USB
              serial (anything else).
    Uplink:   it just forwards from the command PC (TTL serial) to the RF.

    Wiring: TX-RX on USB-TTL converter and TX-RX on board must be crossed.
            Using Serial2 on Feather, it means  USB-TTL TX to pin #11
                                                USB-TTL RX to pin #10

    This program is the Torus version, using TorusRecords. It is based on the
    generic RF-Transceiver template for Cansat projects in CSPU.

    Project not using the RT_CanCommander features do not need to remove anything.
*/

/**----------------------------Includes and defines---------------------------- **/

#include "CansatConfig.h"
#include "TorusRecord.h" 		// $$$ Torus specific
#include "CansatInterface.h"
#include "elapsedMillis.h"
#include "SD_Logger.h"
#include "DoubleSerial.h"
#include "Serial2.h"

#ifdef RF_ACTIVATE_API_MODE
#  include "CansatXBeeClient.h"
#else
#  include "LineStream.h"
#endif

//For debugging
#define DBG_STATION   0
#define DBG_INCOMING_MSG 0
#define DBG_INCOMING_RECORD 0

/**----------------------------Constants---------------------------- **/

constexpr uint32_t RecordFeedbackPeriod=2000;
	// in msec. When receiving records, and no particular condition is detected,
	// only 1 record every RecordFeedbackPeriod msec is displayed on the serial
	// interface.
constexpr bool SendResponseFrameTypeToSerial = true;
	// If false, useless information about the frame type is not sent to serial,
	// in order to improve readability.
	// If true, the frame type is sent to the serial interface when a CmdResponse
	// is received from the can
	// This is used to allow for the RT-Commander GUI interface to
	// discriminate between the strings to be interpreted (the CmdResponse) and
	// the ones to simply display (any other).
//For Hardware
constexpr byte RF_RxPinOnUno = 5; //Rx pin
constexpr byte RF_TxPinOnUno = 6; //Tx pin
constexpr byte TTL_RxPinOnUno = 10; //Rx pin
constexpr byte TTL_TxPinOnUno = 11; //Tx pin

//For the LineStream Class
constexpr unsigned int MaxLineLength = 300; 
  // longest line is the CSV header, keep margin for markers etc..

//For the SD Card
const char * fourCharPrefix = "RfTr";
constexpr byte SD_ChipSelectPinNumber = 13;
constexpr unsigned int requiredFreeMegs = 100;

//For LED's
constexpr byte NumActivityLEDS = 2;		// uplink and downlink.
constexpr byte UplinkIndex = 0;
constexpr byte DownlinkIndex = 1;
constexpr unsigned long LED_StateDuration = 500; // Duration a LED is on or off in msec.
constexpr byte DownlinkLED_Pin 	= 9; 	// Blinks when data is received from the can
constexpr byte UplinkLED_Pin 	= 12;   // Blinks when data sent to the can.
constexpr byte HeartBeatLED = 6;      // Always blinks.
constexpr unsigned long MinActivityDuration = 500; // (msec) the minimum duration the activity is considered on or off.
constexpr byte activityLED_Pin[NumActivityLEDS] = { UplinkLED_Pin, DownlinkLED_Pin} ;
constexpr unsigned long HeartBeatPeriod = 1000; // msec.

/**----------- Initialize the Hardware or Software Serials -----------**/
//Define the different Serials  port
#ifdef SIMULATE_ON_USB_SERIAL
HardwareSerial &RF = Serial;
#else
#  ifdef ARDUINO_SAMD_FEATHER_M0_EXPRESS
HardwareSerial &RF = Serial1;
HardwareSerial &TTL = Serial2;
#   else
#   include "SoftwareSerial.h"
SoftwareSerial RF(RF_RxPinOnUno, RF_TxPinOnUno);
SoftwareSerial TTL(TTL_RxPinOnUno, TTL_TxPinOnUno);
#   endif
#endif

/** ---------------------------- Objects & globals -------------------------------- **/

SD_Logger 		sd(SD_ChipSelectPinNumber);
DoubleSerial 	ds((uint16_t) CansatFrameType::DataRecord);
bool          sdAvailable;

bool linkActive[NumActivityLEDS];
elapsedMillis linkActivityDuration[NumActivityLEDS];  // Durations since the activity last changed state.
elapsedMillis 	ledStateDuration[NumActivityLEDS];    // Durations since the LEDs last changed state.
elapsedMillis heartBeatDelay; // Duration since heartbeat LED last blinking.

#ifdef RF_ACTIVATE_API_MODE
#  ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
#  error "RF_Transceiver requires Feather M0 Express board to use the API mode"
#  endif
CansatXBeeClient xb(CanXBeeAddressSH,CanXBeeAddressSL);
uint8_t *payloadPtr;
uint8_t payloadSize;
char stringReceived[XBeeClient::MaxStringSize+1];
                                // In API mode we need a buffer to extract the string.
TorusRecord recordReceived;     // and a buffer to extract records into.   $$$ Torus-specific
String CSV_Buffer;              // and a buffer to stream records into, as CSV.
StringStream  CSV_BufferStream(CSV_Buffer); // and a stream to fill the above buffer.
   // NB: This buffering system is ugly highly inefficient, but time schedule
   //     does not allow for a redesign...
#else
const char* stringReceived; // In transparent mode, we just need a pointer to the
                            // LineStream internal buffer.
LineStream     ls;
#endif

/** --------------------------- Functions ------------------------------- **/

void printBanner() {
  Serial << "Torus project" << ENDL; // $$$ Torus-specific
  Serial << " ######  #######       ####### " << ENDL;
  Serial << " #     # #                #    #####    ##   #    #  ####   ####  ###### # #    # ###### #####  " << ENDL;
  Serial << " #     # #                #    #    #  #  #  ##   # #      #    # #      # #    # #      #    # " << ENDL;
  Serial << " ######  #####   #####    #    #    # #    # # #  #  ####  #      #####  # #    # #####  #    # " << ENDL;
  Serial << " #   #   #                #    #####  ###### #  # #      # #      #      # #    # #      #####  " << ENDL;
  Serial << " #    #  #                #    #   #  #    # #   ## #    # #    # #      #  #  #  #      #   #  " << ENDL;
  Serial << " #     # #                #    #    # #    # #    #  ####   ####  ###### #   ##   ###### #    # " << ENDL << ENDL;
}

void initAndInfo() {
#ifndef ARDUINO_SAMD_FEATHER_M0_EXPRESS
  Serial << "*** Warning: Not running on Feather M0 Express board ??? ***" << ENDL << ENDL;
#endif
  //SD Card
  sdAvailable = false;
  signed char resultSD_Init = sd.init(fourCharPrefix, "", requiredFreeMegs);
  switch (resultSD_Init) {
    case 0:
      DPRINTS(DBG_DIAGNOSTIC, "  Logger OK. Logging to ");
      DPRINT(DBG_DIAGNOSTIC, sd.fileName());
      DPRINTS(DBG_DIAGNOSTIC, ". ");
      DPRINT(DBG_DIAGNOSTIC, sd.getFreeSpaceInMBytes());
      DPRINTSLN(DBG_DIAGNOSTIC, " Mbytes availaible for storage");
      sdAvailable = true;
      break;
    case 1:
      DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: SD Init successful but required free space not available");
      break;
    case -1:
      DPRINTSLN(DBG_DIAGNOSTIC, "*** Error: SD Init failed. Problem could be: no card, card not formatted, storage full, storage read-only, attempt to initialize twice etc...");
      break;
    default:
      DPRINTS(DBG_DIAGNOSTIC, "*** Unexpected SD init result: ");
      DPRINTLN(DBG_DIAGNOSTIC, resultSD_Init);
      break;
  }

#ifdef RF_ACTIVATE_API_MODE
  CSV_Buffer.reserve(200); // Avoid memory fragmentation.
  xb.begin(RF);
  Serial << "  Using XBee API mode, using XBees set '" << RF_XBEE_MODULES_SET << "'" << ENDL;
 
  Serial << "  Destination (Can) address=0x";
  Serial.print(CanXBeeAddressSH, HEX);
  Serial << "-0x";
  Serial.println(CanXBeeAddressSL,HEX);
#else
  //LineStream
  if (ls.begin(RF, MaxLineLength)) {
    DPRINTSLN(DBG_DIAGNOSTIC, "  LineStream OK.");
  } else {
    DPRINTSLN(DBG_DIAGNOSTIC, "*** Error in LineStream init!");
  }
  Serial << "  Using XBee transparent (AT) mode" << ENDL;
   //DoubleSerial
  ds.begin(TTL);
  DPRINTSLN(DBG_DIAGNOSTIC, "  DoubleSerial OK.");
#endif

  for (int i = 0; i < NumActivityLEDS; i++) {
    ledStateDuration[i] = LED_StateDuration;  // Init to max duration to avoid delaying the first use.
    linkActivityDuration[i] = MinActivityDuration;
    linkActive[i] = false;
  }
  DPRINTSLN(DBG_DIAGNOSTIC, "  Initialization over.");
#ifdef DBG_DIAGNOSTIC
  Serial << "Sending control msg to RT-Processing" << ENDL;
#endif
  TTL << "Link to RT-Processing OK." << ENDL;
  TTL.flush();


#ifdef RF_ACTIVATE_API_MODE
  	 Serial << "Send commands to the can as CSV strings, starting with the CansatCmdRequestType" << ENDL
		    << "followed by the applicable parameters (as defined in CansatInterface.h)" << ENDL;
#else
  	 Serial << "Send commands to the can as CSV strings, starting with "1," followed by the " << ENDL
		    << "CansatCmdRequestType, followed by the applicable parameters (as defined in CansatInterface.h)" << ENDL;
#endif

Serial << "-------------------" << ENDL;
Serial << "Header of expected data record:" << ENDL;
recordReceived.printCSV(Serial, CansatRecord::DataSelector::All, CansatRecord::HeaderOrContent::Header);
Serial << ENDL << "-------------------" << ENDL;
}

void doIdle() {
  sd.doIdle();
}

void setup() {
  //Init the serial ports
  DINIT(USB_SerialBaudRate);
  printBanner();
  RF.begin(RF_SerialBaudRate);
  TTL.begin(USB_SerialBaudRate);
  Serial << "  RF Serial port initialized (" << RF_SerialBaudRate << " baud)" << ENDL;
  Serial << "  RT-Processing serial port initialized (" << USB_SerialBaudRate << " baud)" << ENDL;

  SPI.begin();
  pinMode(SD_ChipSelectPinNumber, OUTPUT);

  //Define LED's on OUTPUT
  pinMode(DownlinkLED_Pin, OUTPUT);
  pinMode(UplinkLED_Pin, OUTPUT);
  pinMode(HeartBeatLED, OUTPUT);

  //Check if all LEDs work
  digitalWrite(DownlinkLED_Pin, HIGH);
  digitalWrite(UplinkLED_Pin, HIGH);
  digitalWrite(HeartBeatLED, HIGH);

  //Inits and info
  initAndInfo();

  delay(1000); // Let the user the time to see if it works properly
  digitalWrite(DownlinkLED_Pin, LOW);
  digitalWrite(UplinkLED_Pin, LOW);
  digitalWrite(HeartBeatLED, LOW);
  Serial << "Initialisation complete. Displaying 1 record every " << RecordFeedbackPeriod/1000.0 << "s." << ENDL << ENDL;
}

/* LED Management:
   In the loop we define whether there is activity on the up- and down- links.
   Activity will never change state more often than every 500 msec.
   LEDs will blink while activity is on.
*/

void setLinkActive(byte idx, bool active) {
  /* If the state is unchanged: reset time if active only.
     If the state is different: change if MinActivityDuration is exceeded
  */
  if (active == linkActive[idx]) {
    if (active) linkActivityDuration[idx] = 0;
  } else {
    if (linkActivityDuration[idx] > MinActivityDuration) {
      linkActive[idx] = active;
      linkActivityDuration[idx] = 0;
    }
  }
} //setDownlinkActive

void updateLEDs() {
  for (int i = 0 ; i < NumActivityLEDS; i ++ ) {
    if (ledStateDuration[i] > LED_StateDuration) {
      ledStateDuration[i] = 0;
      if (linkActive[i]) {
        digitalWrite(activityLED_Pin[i], !digitalRead(activityLED_Pin[i]));
      } else {
        digitalWrite(activityLED_Pin[i], LOW);
      }
    }
  } // for

  if (heartBeatDelay > HeartBeatPeriod) {
    digitalWrite(HeartBeatLED, !digitalRead(HeartBeatLED));
    heartBeatDelay = 0;
  }
}

// Provide some feedback after a record was received to keep the user informed.
void provideUserFeedbackOnRecord() {
	static uint32_t lastReceivedTS=0;
	static uint32_t lastFeedbackTS=0;
	static uint32_t currentRecordPeriod=0;

	uint32_t newPeriod = recordReceived.timestamp - lastReceivedTS;
	uint32_t now=millis();
	bool timeForFeedback=(now - lastFeedbackTS) >= RecordFeedbackPeriod;

	if (newPeriod != currentRecordPeriod) {
		currentRecordPeriod=newPeriod;
		timeForFeedback=true;
	}

	// Provide some feedback every 2 secs
	if (timeForFeedback) {
		recordReceived.printCSV(Serial);
		Serial << " ... (delay since last = " << newPeriod << " msec)" << ENDL;
		lastFeedbackTS=now;
	}
	lastReceivedTS=recordReceived.timestamp;
}

#ifdef RF_ACTIVATE_API_MODE
bool receiveFrame(bool& receivedA_String, bool& receivedA_Record) {
	receivedA_String=false;
	receivedA_Record=false;
	CansatFrameType stringType;
	static int currentStringPartIdx=-1; // keep track: if we are receiving string parts, we should not
										// insert newline characters unless a new string is started.
	uint8_t stringSeqNumber;
	bool gotRecord;
	if (xb.receive(recordReceived, stringReceived, stringType, stringSeqNumber, gotRecord)) {
		if (gotRecord) {
			receivedA_Record = true;
			if (currentStringPartIdx>=0) {
				// We were in a multi-part string and it is now over.
				Serial << ENDL;
				if (sdAvailable) {
					sd.log("", true);
				}
				currentStringPartIdx=-1;
			}
			provideUserFeedbackOnRecord();
		} else
		{
			switch (stringType) {
			case CansatFrameType::CmdRequest:
						DPRINTSLN(DBG_DIAGNOSTIC,
								"*** Received a Cmd Request ?? Ignored.")
								;
			break;
			case CansatFrameType::CmdResponse:
			case CansatFrameType::StatusMsg:
			case CansatFrameType::StringPart:
				receivedA_String = true;
			break;
			default:
				DPRINTS(DBG_DIAGNOSTIC,"*** Error: unexpected CansatFrameType received with string (ignored):");
				DPRINTLN(DBG_DIAGNOSTIC, (int) stringType);
			} // switch
		} // gotRecord
	} // anything received


	//If anything valid received, store the data and give it to the computer
	if (receivedA_String) {
		bool endlBefore=false;
		bool endlAfter=false;
		bool newStringStarted=true;
		DPRINTS(DBG_INCOMING_MSG, "Received string: type=");
		DPRINT(DBG_INCOMING_MSG, (int) stringType);
		DPRINTS(DBG_INCOMING_MSG, ": ");
		DPRINTSLN(DBG_INCOMING_MSG, stringReceived);

		// String always go to serial, only records go to TTL
		// Add an ENDL after a string we received, unless it is a StringPart
		// Add an ENDL before a string we received if  (i) we were receiving string parts
		// and (ii) it is NOT a string part or it is a string part with sequence number lower
		// than the previous one.
		// We need to prepend every string with "* " unless it is
		// not the first part of a string.
		if (stringType == CansatFrameType::StringPart){
			if (currentStringPartIdx < 0) {
				// This is the first part of a string, and the previous one was not a string part
				// No ENDL required.
				currentStringPartIdx=stringSeqNumber;
				newStringStarted=true;
			} else if (stringSeqNumber <= currentStringPartIdx) {
				// This is the first part of a string, and the previous one was the last part of
				// another string. Separate with a ENDL;
				endlBefore=true;
				newStringStarted=true;
				currentStringPartIdx=stringSeqNumber;
			}
			else if(stringSeqNumber == (currentStringPartIdx+1)) {
				// This is the next part of the current string. No ENDL required
				currentStringPartIdx++;
				newStringStarted=false;
			}
			else {
				// We missed some parts, and assume we are still in the same string.
				Serial << ENDL << "*** Error: missing string parts " << currentStringPartIdx+1 << " to "
					   << stringSeqNumber-1 <<"!" << ENDL;
				newStringStarted=true;
				currentStringPartIdx=stringSeqNumber;
			}
		} else {
			newStringStarted=true;
			// This is not a string part. If the previous one was a string part, terminate it with ENDL;
			// Terminate this one with ENDL;
		   if (currentStringPartIdx >=0) {
			   endlBefore=true;
			   currentStringPartIdx=-1;
		   }
		   endlAfter=true;
		}

		//Store data to the SD card
		if (sdAvailable) {
			DPRINTSLN(DBG_STATION, "Storing on SD Card...");

			if (endlBefore) {
				sd.log("", true);
			}
			sd.log(stringReceived, endlAfter);
		} else {
			DPRINTSLN(DBG_STATION, "*** Skipping string storage: sd not available ***");
		}
		//Send to serial
		if (endlBefore) Serial << ENDL;
		if (SendResponseFrameTypeToSerial && (stringType == CansatFrameType::CmdResponse)) {
			Serial << (int) CansatFrameType::CmdResponse << ",";
		} else if (newStringStarted==true) {
			Serial << "* ";
		}

		// Print string adding "* " after each newline inside the string
		char * current = strtok(stringReceived, "\n");
		// The first token, we print anyway: it goes either to the end of the string
		// or to the first '\'.
		Serial << current;
		// Then we print all subsequent tokens on a new line, prepended with "*_"
		current=strtok(nullptr,"\n");
		while (current) {
		   Serial << ENDL << "* " << current;
		   current=strtok(nullptr,"\n");
		}
		if (endlAfter) Serial << ENDL;
	}
	else if (receivedA_Record){
		DPRINTSLN(DBG_INCOMING_RECORD, "Received a record:");
		// Convert to CSV string for storage & transmission
		recordReceived.printCSV(CSV_BufferStream);
		if (sdAvailable) {
			//Store data to the SD card
			DPRINTSLN(DBG_STATION, "Storing on SD Card...");
			sd.log(CSV_Buffer, true);
		} else {
			DPRINTSLN(DBG_STATION, "*** Skipping record storage: sd not available ***");
		}
		//Using DoubleSerial
		DPRINTSLN(DBG_STATION, "Sending the received record to the serial ports...");
		DPRINTLN(DBG_INCOMING_RECORD, CSV_Buffer.c_str());
		TTL << CSV_Buffer.c_str() << ENDL;
		CSV_Buffer="";
	}
	return (receivedA_String || receivedA_Record);
}
#endif

void loop() {
  bool downlinkActive = false;
  bool receivedA_String=false;
  bool receivedA_Record=false;
  bool uplinkActive = false;

  // **** Part 1: downlink ****
  // Read one message from the radio
#ifdef RF_ACTIVATE_API_MODE
  downlinkActive =receiveFrame(receivedA_String, receivedA_Record);
#else
  // In transparent mode, just read from a stream.
  // (returns NULL if no end-of-line or end-of-string marker is received)
  stringReceived = ls.readLine();
  receivedA_String= (stringReceived != nullptr);
  receivedA_Record= false; // in transparent mode, records are received as strings.

  //If anything received, store the data and give it to the computer
  if (receivedA_String) {
    downlinkActive=true;
    DPRINTS(DBG_INCOMING_MSG, "Received  string: ");
    DPRINTSLN(DBG_INCOMING_MSG, stringReceived);
    if (sdAvailable) {
      //Store data to the SD card
      DPRINTSLN(DBG_STATION, "Storing on SD Card...");
      sd.log(stringReceived, true);
    } else {
      DPRINTSLN(DBG_STATION, "*** Skipping string storage: sd not available ***");
    }
    //Using DoubleSerial
    DPRINTSLN(DBG_STATION, "Sending the received string to the serial ports...");
    ds << stringReceived;
  }
#endif
  setLinkActive(DownlinkIndex, downlinkActive);

  // *** Part 2: uplink
  uplinkActive = Serial.available();
  if (uplinkActive) {
    Serial << ENDL << "--->"; // Start with ENDL: we could be in a stringPart sequence.
    char c=' ';
    RF_OPEN_CMD_REQUEST((&xb));
    while (Serial.available()) {
    	c = (char) Serial.read();
    	// We do not transmit end-of-line characters.
    	if ((c != '\n') && (c !='\r'))  {
#ifdef RF_ACTIVATE_API_MODE
       xb << c;
#else
       RF << c;
#endif
       }
       Serial << c; // Echo on terminal
    }
    RF_CLOSE_CMD_REQUEST((&xb));
    if ((c != '\n') && (c != '\r')) {
    	// If the user didn't send an EOL, add one in the display.
    	Serial << ENDL;
    }
    setLinkActive(UplinkIndex, true);
  }	else {
    setLinkActive(UplinkIndex, false); 
  }

  // If it is an empty loop, check for maintenance tasks
  if (!uplinkActive && !downlinkActive) {
    doIdle();
  }

  updateLEDs(); // Blink LEDs as required.
}
