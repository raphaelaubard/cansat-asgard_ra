/*
   SSC_ThermistorClient.cpp
*/

#include "SSC_ThermistorClient.h"

SSC_ThermistorClient::SSC_ThermistorClient(float Vcc, byte NTCLE_pin, byte NTCLG_pin, byte VMA_pin,
		 	 	 	 	 	 	 	 	 	 uint32_t NTCLE_Resistor,
											 uint32_t NTCLG_Resitor,
											 uint32_t VMA_Resistor ):
  thermistor1(Vcc, NTCLE_pin, NTCLE_Resistor),
  thermistor2(Vcc, NTCLG_pin, NTCLG_Resitor),
  thermistor3(Vcc, VMA_pin, VMA_Resistor)
{}

bool SSC_ThermistorClient::readData(SSC_Record& record) {
  record.temperatureThermistor1 = thermistor1.readTemperature();
  record.temperatureThermistor2 = thermistor2.readTemperature();
  record.temperatureThermistor3 = thermistor3.readTemperature();
  return true;
}
