/*
   Run the IMU Calibration and Madgwick quaternion update on-board, as fast as possible.
   Tests results on FeatherBoard: period=4.48 msec with just the IMU reading/calibration/fusion.

   See IMU_FusionFilter.cpp for a number of useful debugging options.
*/

#define DEBUG_CSPU
#include "DebugCSPU.h"
#include "IMU_FusionFilter.h"
#include "elapsedMillis.h"

constexpr unsigned long UpdateDelay = 100; // msec. Delay between successive IMU readings.
constexpr unsigned long OutputDelay = 100; // msec. Delay between successive Euler angles output.

float storedData[26][4];    // Data stored
unsigned long nb = 0;

//#define DEBUG_TIMING           // Define to see the timing (pollutes the output for the RT-GUI...
//#define OUTPUT_TO_RT_GUI        // Define to output complete ground records for the RT-GUI.
#define OUTPUT_EULER_ANGLES   // Defined to output the Euler angles in human-readable format.
//#define MagDirection            // Defined to ouput the direction from the magnetometer.
#define STORE_DATA               // Defined to store the computed angles (pollutes the output for the RT-GUI...
// NB: To output calibrated IMU readings, define DBG_CALIBRATION to 1 in IMU_FusionFilter.cpp

IMU_FusionFilter filter;
elapsedMillis elapsed, elapsedSinceTransfer;
unsigned long counter = 0;

void setup() {
  DINIT(115200);
  if (!filter.begin(UpdateDelay)) {
    Serial << "Error during IMU_FusionFilter init" << ENDL;
    while (1) {
      delay(100);
    }
  }
  elapsed = elapsedSinceTransfer = 0;
  Serial << "Setup done" << ENDL << ENDL;
}

void loop() {
  if (elapsed >= UpdateDelay) {
    elapsed = 0;
    filter.update();
    counter++;
  }
  if (elapsedSinceTransfer >= OutputDelay) {
    unsigned long ts = elapsedSinceTransfer;
#ifdef DEBUG_TIMING
    Serial << "Updated " << counter << " times in " << ts << " msec. Period = " << ((float) ts) / counter << " msec." << ENDL;
#endif
    elapsedSinceTransfer = 0;
    counter = 0;
#ifdef OUTPUT_TO_RT_GUI
    // Output as IsaTwoGroundRecord for feeding the RT-GUI.
    Serial << "100," << millis() << ',';
    for (int i = 0; i < 18 ; i++ ) Serial << "0,"; // Complete IsaTwoRecord.
    for (int i = 0; i < 6 ; i++ ) Serial << "0,"; // Start GroundRecord
    for (int i = 0; i < 3 ; i++ ) Serial << filter.calibratedMag[i] << ","; // CalibratedMag
    for (int i = 0; i < 3 ; i++ ) Serial << "0,";
    Serial << filter.getRoll() << ",";
    Serial << filter.getYaw() << ",";
    Serial << filter.getPitch() << ",";
    for (int i = 0; i < 24 ; i++ ) Serial << "0,"; // Terminate GroundRecord
#ifdef MagDirection
    float direction = atan(filter.calibratedMag[1] / filter.calibratedMag[0])* 180 / PI;
    Serial << direction;
#endif
    Serial << ENDL;

#endif
#ifdef OUTPUT_EULER_ANGLES
    Serial << "R: " << filter.getRoll() << " (" << filter.getRoll() * 180.0 / PI << "°) / ";
    Serial << "Y: " << filter.getYaw() << " (" << filter.getYaw() * 180.0 / PI << "°) / ";
    Serial << "P: " << filter.getPitch() << " (" << filter.getPitch() * 180.0 / PI << "°)" << ENDL;
#endif

  }
#ifdef STORE_DATA
  if (Serial.available()) {
    int inbyte = Serial.read();
    if (inbyte == '1' && nb < 25) {        //Stores data
      storedData[nb][0] = filter.getRoll();
      storedData[nb][1] = filter.getYaw();
      storedData[nb][2] = filter.getPitch();
      #ifdef MagDirection
      storedData[nb][3] = atan(filter.calibratedMag[1] / filter.calibratedMag[0])* 180 / PI;
      #endif
      nb++;
      Serial << "Data stored!" << ENDL;
      if(nb == 25){
        Serial << ENDL << "-----MAX REACHED-----" << ENDL << ENDL;
      }
    }
    else if(inbyte == '1' && nb >= 25){
      Serial << ENDL << "-----MAX REACHED-----" << ENDL << ENDL;
    }
    else if (inbyte == '2') {           //Output data stored
      Serial << "#Test Result" << ENDL;
      for (int i = 0; i < nb; i++) {
        #ifndef MagDirection
        for (int y = 0; y < 3; y++) Serial << storedData[i][y] << ",";
        #else
        for (int y = 0; y < 4; y++) Serial << storedData[i][y] << ",";
        #endif
        Serial << ENDL;
      }
      nb = 0;
    }
    else if (inbyte == '3') {       //Reset the data stored
      nb = 0;
      memset(storedData, 0,sizeof(storedData));
      Serial << "***** The data has been cleared *****" << ENDL << ENDL;
    }
  }
#endif
}
