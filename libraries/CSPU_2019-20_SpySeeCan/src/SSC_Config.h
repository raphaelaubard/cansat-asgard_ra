/*
   System-wide configuration for the SpySeeCan Project
*/

#pragma once


#include "Arduino.h"
#include "limits.h"

// ********************************************************************
// ***************** Calibration settings to update frequently ********
// ********************************************************************

constexpr float SeaLevelPressure_HPa = 1010.7;  // Sea level pressure value to calculate actual altitude.

enum class SSC_RecordType {
  // DO NOT USE VALUE 0 (value returned when string to int conversion fails).
  DataRecord = 12,              /**< Value denoting a measurement record */
  CmdRequest = 1,              /**< Value denoting a command (request)  */
  CmdResponse = 2,             /**< Value denoting a command response   */
  StatusMsg = 3,                 /**< Value denoting an unsollicitated status message from the can
                                                  Format of the record, after the RecordType field:
                                                        - timestamp
                                                        - status message (string until end of record) */
  StringPart = 4,        /**< Value denoting a part of a string. The receiver should
                          not include any end-of-line between string parts */
  GroundDataRecord = 100       /**< Value denoting a ground record */
};

// ********************************************************************
// **************************** DEBUG SETTINGS ************************
// ********************************************************************
#define IGNORE_EEPROMS             // When defined, detected EEPROM chips are ignored, in order to avoid wasting write cycles during tests
// UNDEFINE THIS SYMBOL FOR OPERATIONS (when EEPROMs are used).
#define NO_EEPROMS_ON_BOARD		   // Define to turn off all EEPROM-related features.

#define PRINT_DIAGNOSTIC_AT_INIT   //Comment out to avoid startup diagnostic if memory usage must be reduced.

// Comment definition out to reduce program size if there is no use
// for the serial port at all (no debugging, no diagnostic at startup, etc.).
// AcquisitionProcess
#define PRINT_ACQUIRED_DATA       // Define to have data printed in very readable format (DEBUGGING ONLY)
#define PRINT_ACQUIRED_DATA_CSV     // Define to have data printed in CSV (1 line/record) format (DEBUGGING ONLY)
//#define START_CAMPAIGN_IMMEDIATELY  //Define to start campaign immediately (DEBUGGING ONLY)

#define DEBUG_CSPU                // Comment out to avoid including any debugging code. DEFINE FOR OPERATION
#define USE_ASSERTIONS           // UNDEFINE THIS SYMBOL FOR OPERATIONS.
#define USE_TIMER                // Comment out to avoid timing output and overhead. UNDEFINE THIS SYMBOL FOR OPERATIONS
#include "DebugCSPU.h"             // include AFTER defining/undefining DEBUG_CSPU, USE_ASSERTION and USE_TIMER.

#ifdef USE_TIMER
#include "Timer.h"
#endif

// Enable/disable the various parts of the debugging here:
// 1. This group of debug settings must be set to 0 for operation
#define DBG_SETUP 1
#define DBG_INIT  1
#define DBG_LOOP  0
#define DBG_LOOP_MSG  1
#define DBG_ACQUIRE 1
#define DBG_STORAGE 1
#define DBG_SLOW_DOWN_TRANSMISSION 0   // Add a 500 msec delay in the RF transmission
#define DBG_CHECK_SPI_DEVICES 1	  // SSC_HW_Scanner: print detection of SPI device.
#define DBG_SLAVE_SYNC 1		  // Document every resynchronisation of slave clock.

// 2. This group of debug settings should preferably be set to 1 for operation,
//    unless there is a memory shortage.
#define DBG_CAMPAIGN_STARTED 1

// ********************************************************************
// ************************ Mission parameters ************************
// ********************************************************************

// 1. For the master controller.

constexpr unsigned int SSC_AcquisitionPeriod = 70;  	  // Acquisition period in msec. If RF_TransmissionDelayEveryTwoNumbers>0,
// check carefully the actual duration of the acquisition+storage+transmission.
// Ditto for PRINT_ACQUIRED_DATA or any time-consuming feature.
constexpr byte imuSamplesPerReading = 1;				 // Number of samples to average at each IMU reading.
// Keep to 1, increasing seems to degrade the result.
constexpr unsigned int SSC_CampainDuration = 300;   // Expected duration of the measurement campaign in seconds.
constexpr unsigned int SSC_SensorsWarmupDelay = 1000; // Delay in msec to allow for the sensors to be ready.
constexpr byte SSC_PrintEvery_X_Records = 50; // If campaign not started, 1 out of how many records should be sent on RF (and printed)

// Detection of take-off ("start campaign condition")
constexpr float AltitudeThresholdToStartCampaign = 600.0f;
// The altitude above which the campaign is considered as
// started, whatever the speed conditions.
constexpr float MinSpeedToStartCampaign = 3.0f ;       // Minimum speed (m/s) to detect during NumSamplesToStartCampaign consecutive
// samples. Warning: MinSpeedToStartCampaign may not be > 10 m/s.
constexpr byte NumSamplesToAverageForStartingCampaign = (1000 / SSC_AcquisitionPeriod);
// Number of samples used to average the vertical velocity to detect the take off.
// WARNING: sensor accuracy is +- 1 m/s, so noise cause detection of
// apparent speed of 1/acquisitionPeriod m/S. For
// acquisition period = 200 msec, this can be 5 m/s ! It is therefore
// essential to have NumSamplesToStartCamapaign cover at least 1 second.


constexpr unsigned int SD_RequiredFreeMegs = 0;     // The number of free Mbytes required on the main SD card at startup (0=do not check).
constexpr uint16_t EEPROM_KeyValue = 0x1217;          // The key identifying a valid EEPROM header.

constexpr byte numDecimalPositionsToUse = 5; 	 // The number of decimal positions to use in string representations of acquired float data.
constexpr byte maxUplinkMsgLenght = 100;		 // The maximum number of characters in an uplink message (cmd to the RT-Commander).
constexpr unsigned long RT_CommanderTimeoutInMsec = 15000L; // The inactivity duration after which the can resumes the acquisition mode
constexpr unsigned long ReportingPeriodWhileWaitingForStartCampaign = 400; // msec. Period for reporting average speed and other
// parameters while waiting for take-off.

#define RF_ACTIVATE_API_MODE		// Define to perform all RF communication in API mode.
#define RF_XBEE_MODULES_SET	'A'		// Define for the XBee pair in used.
// Valid values: 'A', 'B', or 'T' (=TEST)
#ifdef RF_ACTIVATE_API_MODE
constexpr uint8_t RF_LongestStringLength = 200;
#  if (RF_XBEE_MODULES_SET=='A')
constexpr uint32_t CanXBeeAddressSH = 0x0013a200; // Address of can XBee module (API mode)
constexpr uint32_t CanXBeeAddressSL = 0x418FC759;
constexpr uint32_t GroundXBeeAddressSH = 0x0013a200; // Address of RF-Transceiver XBee module (API mode)
constexpr uint32_t GroundXBeeAddressSL = 0x415E655F;
#  elif (RF_XBEE_MODULES_SET=='B')
constexpr uint32_t CanXBeeAddressSH = 0x0013a200; // Address of can XBee module (API mode)
constexpr uint32_t CanXBeeAddressSL = 0x418FBBEB;
constexpr uint32_t GroundXBeeAddressSH = 0x0013a200; // Address of RF-Transceiver XBee module (API mode)
constexpr uint32_t GroundXBeeAddressSL = 0x418FC78B;
#  elif (RF_XBEE_MODULES_SET=='T')					// Test modules
constexpr uint32_t CanXBeeAddressSH = 0x0013a200; // Address of can XBee module (API mode)
constexpr uint32_t CanXBeeAddressSL = 0x41827f67; // The one without antenna
constexpr uint32_t GroundXBeeAddressSH = 0x0013a200; // Address of RF-Transceiver XBee module
constexpr uint32_t GroundXBeeAddressSL = 0x418fb90a; // (the one with antenna)
#  else
#    error "Unknown XBee set in SSC_Config.h"
#  endif // RF_USE_FINAL_XBEE_MODULES
#endif // RF_ACTIVATE_API_MODE

constexpr unsigned int SSC_GPS_Frequency = 5; // (Hz) ! must be either 1, 5 or 10 !


// ********************************************************************
// ********************* Hardware configuration  **********************
// ********************************************************************


#undef SSC_USE_EEPROMS            // Undefine if the can does not include EEPROMS.

// Serial ports
constexpr  long USB_SerialBaudRate = 115200;              // baudrate on the serial interface of the µC board. Faster is best.
constexpr  long RF_SerialBaudRate  = 115200;              // baudrate to communicate with the RF board (must be consistent with XBee settings.
constexpr  byte GPS_SerialPortNumber = 2;
constexpr  byte RF_SerialPortNumber  = 1;

// I2C Slaves
// Two possible IMU boards:
constexpr byte I2C_LSM9DS0_Address = 0x1D; 		// LSM9DS0 Accel+Magnetometer (gyro on 0x6B)
constexpr byte I2C_LSM9DS0_SecondaryAddress = 0x6B;
constexpr byte I2C_PrecisionNXP_Address = 0x1F; // Precision NXP Accel+Magnetometer (gyro on 0x21)
constexpr byte I2C_PrecisionNXP_SecondaryAddress = 0x21;

// Actual addresses used.
#if (IMU_REFERENCE == NXP_A) || (IMU_REFERENCE == NXP_B)
#  define USE_NXP_PRECISION_IMU		// NXP Precision IMU is used
#elif (IMU_REFERENCE == LSM_A) || (IMU_REFERENCE == LSM_B) || (IMU_REFERENCE == LSM_C)
#  undef USE_NXP_PRECISION_IMU		// LSM9DS0 IMU is used
#else
#  error "Unexpected IMU_REFERENCE value in SSC_Config.h"
#endif

constexpr byte I2C_lowestAddress = 0x10;
constexpr byte I2C_highestAddress = 0x7F;   // EEPROMS are at 0x50-0x57.
constexpr byte I2C_BMP_SensorAddress = 0x77;

// Analog pins allocation
constexpr byte ThermistorNTCLE_AnalogInPinNbr = A0 ;     // Analog input for thermistor
constexpr byte ThermistorNTCLG_AnalogInPinNbr = A1 ;     // Analog input for thermistor
constexpr byte ThermistorVMA_AnalogInPinNbr = A2 ;     // Analog input for thermistor
constexpr byte unusedAnalogInPinNumber = A3; // An unused analog pin. Value read is used as random seed.
constexpr byte DebugCtrlPinNumber = A5; 	 // The pin used to force the initialisation of the USB link.

#if (defined ARDUINO_AVR_UNO || defined ARDUINO_AVR_MICRO)
// This is an alternate configuration for development using a standard UNO board.
constexpr byte pin_HeartbeatLED = LED_BUILTIN;
constexpr byte pin_InitializationLED = 8;
constexpr byte pin_AcquisitionLED = 0;
constexpr byte pin_StorageLED = 0;
constexpr byte pin_TransmissionLED = 0;
constexpr byte pin_CampaignLED = 8;
constexpr byte pin_UsingEEPROM_LED = 0;
constexpr int defaultDAC_StepNumber = 1023;
constexpr float referenceVoltage = 5;
// SPI Slaves
constexpr byte SD_CardChipSelect = 10;  // Master µC SD-card (not actually on board with Uno)
constexpr byte NumSI4432_Cards=1;
constexpr byte SI4432_ChipSelect[]= { 5 } ;
constexpr byte NumFrequencyBand=9;
constexpr float FrequencyStep=1.0;
constexpr uint16_t StartFrequency[] = { 433, 478, 523, 568, 613, 658, 703, 748, 793 };


#elif defined ARDUINO_SAMD_FEATHER_M0_EXPRESS
// This is the operational target hardware.
constexpr byte pin_HeartbeatLED = LED_BUILTIN;
constexpr byte pin_InitializationLED = 12; // On during initialization
constexpr byte pin_AcquisitionLED = 0;	  // Our cycle is too short to use all LEDs.
constexpr byte pin_StorageLED = 0;
constexpr byte pin_TransmissionLED = 0;
constexpr byte pin_CampaignLED = 8;		  // On until campaign started. Built-in green led on Adalogger
constexpr byte pin_UsingEEPROM_LED = 0;
constexpr int defaultDAC_StepNumber = 4095;
constexpr float referenceVoltage = 3.3;
//  SPI Slaves
constexpr byte SD_CardChipSelect = 4;   // On-board SD-card on Adalogger
constexpr byte NumSI4432_Cards=1;		// Number of SI4432 modules onboard.
constexpr byte SI4432_ChipSelect[]= { 5 } ;
constexpr byte NumFrequencyBand=9;
constexpr float FrequencyStep=1.0;
constexpr uint16_t StartFrequency[] = { 433, 478, 523, 568, 613, 658, 703, 748, 793 };
#else
#error "Unexpected board. Please use either TMinus, Feather M0 Express or Uno, or complete the configuration file "
#endif
