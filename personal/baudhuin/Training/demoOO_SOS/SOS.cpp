
#define DEBUG_CSPU
#include "DebugCSPU.h"

#define DBG_FSM 1 

#include "SOS.h"


SOS::SOS(byte thePinNumber)
  : pinNumber(thePinNumber),
    state(SOS_State_t::firstS_Character),
    counter(0),
    ts(millis())
{
  pinMode(pinNumber, OUTPUT);
  startThreeSigns();
}

void SOS::startThreeSigns() {
  counter = 0;
  digitalWrite(pinNumber, HIGH);
  ts = millis();
}

bool SOS::threeSigns(unsigned long duration) {
  unsigned long elapsed = millis() - ts;
  bool done = false;
  if (digitalRead(pinNumber) == HIGH) {
    // LED is on: turn off after duration
    if (elapsed >= duration) {
      digitalWrite(pinNumber, LOW);
      ts = millis();
    }
  }
  else {
    // LED is off: sign will be over after delayBetweenSigns msec
    if (elapsed >= delayBetweenSigns) {
      counter++;
      if (counter < 3) {
        digitalWrite(pinNumber, HIGH);
        ts = millis();
      }
      else done = true;
    }
  } // else

  return done;
}

void SOS::run() {
  switch (state) {
    case SOS_State_t::firstS_Character:
      if (threeSigns(dotDuration)) {
        DPRINTS(DBG_FSM, "1st S over at ");
        DPRINTLN(DBG_FSM,millis());
        startThreeSigns();
        state = SOS_State_t::O_Character;
      }
      break;
    case SOS_State_t::O_Character:
      if (threeSigns(dashDuration)) {
        DPRINTS(DBG_FSM, "1st 0 over at ");
        DPRINTLN(DBG_FSM,millis());
        startThreeSigns();
        state = SOS_State_t::secondS_Character;
      }
      break;
    case SOS_State_t::secondS_Character:
      if (threeSigns(dotDuration)) {
        DPRINTS(DBG_FSM, "2nd S over at ");
        DPRINTLN(DBG_FSM,millis());
        state = SOS_State_t::waiting;
      }
      break;
    case SOS_State_t::waiting:
      if ((millis() - ts) >= delayBetweenSOS) {
        DPRINTS(DBG_FSM, "Waiting over at ");
        DPRINTLN(DBG_FSM,millis());
        startThreeSigns();
        state = SOS_State_t::firstS_Character;
        // do not update ts, so the next call will switch LED on.
      }
      break;
    default:
      DASSERT(false);
  }
}
