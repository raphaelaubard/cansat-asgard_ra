#include "servoLatch.h"

ServoLatch srv;


void setup() {
  Serial.begin(115200);
  srv.begin(9);
}

void loop() {
  Serial << "UnLocking..." <<ENDL;
         srv.unlock();
  delay(3000);
  Serial << "Locking..."<<ENDL;
         srv.lock();
  delay(3000);

}
